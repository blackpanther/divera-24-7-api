#!/bin/bash
CONFIGURATION=Release

cd "$(dirname "${0}")"
cd ".."
rm -rf artifacts
mkdir artifacts

dotnet restore -v q
# dotnet tool restore -v q

pushd AMWD.Net.Api.Divera
rm -rf bin
dotnet build -c ${CONFIGURATION} --nologo --no-incremental
mv bin/${CONFIGURATION}/*.nupkg ../artifacts
mv bin/${CONFIGURATION}/*.snupkg ../artifacts
popd
