# Divera 24/7 API

The library/package implements the API provided by Divera 24/7 to create alarms and communicate with the app/platform.

Sources:
- Application: [divera247.com](https://www.divera247.com/)
- API (Swagger UI): [api.divera247.com](https://api.divera247.com/)

---

**LICENSE:** [MIT](LICENSE.txt)
