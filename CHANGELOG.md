# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://git.am-wd.de/andreasmueller/divera-24-7-api/compare/v1.1.0...main) - 0000-00-00
_nothing changed yet_


## [v1.1.0](https://git.am-wd.de/andreasmueller/divera-24-7-api/compare/v1.0.0...v1.1.0) - 2022-07-16
### Changed
- Moved exceptions to sub-namespace "Exceptions"

### Removed
- Support for external `HttpClient`


## [1.0.0](https://git.am-wd.de/andreasmueller/divera-24-7-api/compare/v0.2.0...v1.0.0) - 2022-01-14
### Added
- `CreateMessage` method
- `ConfirmEvent` method
- `ConfirmSurvey` method
- `ConvertCoordinates` method
- `PushToNavi` method
- `SetVehicleStatus` method
- `SetUserStatus` method
- `GetUsers` method
- `TestPush` method

### Changed
- Using `ICollection<string>` instead of `IEnumerable<string>` on `CreateAlarmRequest`
- Throwing a `DiveraException` on error response

### Fixed
- Requests are performed with UrlEncoded strings instead of JSON (required by Divera24/7)


## [v0.2.0](https://git.am-wd.de/andreasmueller/divera-24-7-api/compare/v0.1.1...v0.2.0) - 2022-01-10
### Changed
- `CreateAlarmRequest.AlarmcodeId` is a single and no list


## [v0.1.1](https://git.am-wd.de/andreasmueller/divera-24-7-api/compare/v0.1.0...v0.1.1) - 2022-01-09
### Added
- Changelog
- UnitTests

### Changed
- CI Build with integrated tests
- Methods have an optional `CancellationToken`

### Fixed
- `SetAlarmConfirmation` never succeeded due to missing serialized content


## [v0.1.0](https://git.am-wd.de/andreasmueller/divera-24-7-api/commits/v0.1.0) - 2022-01-08

Basic implementation
