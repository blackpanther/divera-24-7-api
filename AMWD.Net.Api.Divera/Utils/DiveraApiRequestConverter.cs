﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using AMWD.Net.Api.Divera.Enums;
using AMWD.Net.Api.Divera.Requests;

namespace AMWD.Net.Api.Divera.Utils
{
	internal static class DiveraApiRequestConverter
	{
		public static HttpContent ConvertToHttpContent(this CreateAlarmRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			var formData = new FormData();

			if (!string.IsNullOrWhiteSpace(request.Number))
				formData.Append("number", request.Number.ToString());

			if (!string.IsNullOrWhiteSpace(request.Title))
				formData.Append("title", request.Title.Trim());

			if (request.HasPriority.HasValue)
				formData.Append("priority", (request.HasPriority.Value ? 1 : 0).ToString());

			if (!string.IsNullOrWhiteSpace(request.Text))
				formData.Append("text", request.Text.Trim());

			if (!string.IsNullOrWhiteSpace(request.Address))
				formData.Append("address", request.Address.Trim());

			if (request.Latitude.HasValue)
				formData.Append("lat", request.Latitude.Value.ToString(CultureInfo.InvariantCulture));

			if (request.Longitude.HasValue)
				formData.Append("lng", request.Longitude.Value.ToString(CultureInfo.InvariantCulture));

			if (!string.IsNullOrWhiteSpace(request.SceneObject))
				formData.Append("scene_object", request.SceneObject.Trim());

			if (!string.IsNullOrWhiteSpace(request.Caller))
				formData.Append("caller", request.Caller.ToString());

			if (!string.IsNullOrWhiteSpace(request.Patient))
				formData.Append("patient", request.Patient.Trim());

			if (!string.IsNullOrWhiteSpace(request.Remark))
				formData.Append("remark", request.Remark.Trim());

			if (!string.IsNullOrWhiteSpace(request.Units))
				formData.Append("units", request.Units.Trim());

			if (request.HasDestination.HasValue)
				formData.Append("destination", (request.HasDestination.Value ? 1 : 0).ToString());

			if (!string.IsNullOrWhiteSpace(request.DestinationAddress))
				formData.Append("destination_address", request.DestinationAddress.Trim());

			if (request.DestinationLatitude.HasValue)
				formData.Append("destination_lat", request.DestinationLatitude.Value.ToString(CultureInfo.InvariantCulture));

			if (request.DestinationLongitude.HasValue)
				formData.Append("destination_lng", request.DestinationLongitude.Value.ToString(CultureInfo.InvariantCulture));

			if (!string.IsNullOrWhiteSpace(request.AdditionalText1))
				formData.Append("additional_text_1", request.AdditionalText1.Trim());

			if (!string.IsNullOrWhiteSpace(request.AdditionalText2))
				formData.Append("additional_text_2", request.AdditionalText2.Trim());

			if (!string.IsNullOrWhiteSpace(request.AdditionalText3))
				formData.Append("additional_text_3", request.AdditionalText3.Trim());

			if (request.Rics?.Any() == true)
				formData.Append("ric", string.Join(",", request.Rics));

			if (request.Vehicles?.Any() == true)
				formData.Append("vehicle", string.Join(",", request.Vehicles));

			if (request.Persons?.Any() == true)
				formData.Append("person", string.Join(",", request.Persons));

			if (request.DelaySeconds.HasValue)
				formData.Append("delay", request.DelaySeconds.ToString());

			if (!string.IsNullOrWhiteSpace(request.Coordinates))
				formData.Append("coordinates", request.Coordinates);

			if (request.CoordinatesX.HasValue)
				formData.Append("coordinates-x", request.CoordinatesX.Value.ToString(CultureInfo.InvariantCulture));

			if (request.CoordinatesY.HasValue)
				formData.Append("coordinates-y", request.CoordinatesY.Value.ToString(CultureInfo.InvariantCulture));

			if (request.IsNews.HasValue)
				formData.Append("news", (request.IsNews.Value ? 1 : 0).ToString());

			if (request.Id.HasValue)
				formData.Append("id", request.Id.ToString());

			if (request.ClusterIds?.Any() == true)
			{
				foreach (int clusterId in request.ClusterIds)
					formData.Append("cluster_ids", clusterId.ToString());
			}

			if (request.GroupIds?.Any() == true)
			{
				foreach (int groupId in request.GroupIds)
					formData.Append("group_ids", groupId.ToString());
			}

			if (request.UserClusterRelationIds?.Any() == true)
			{
				foreach (int relationId in request.UserClusterRelationIds)
					formData.Append("user_cluster_relation_ids", relationId.ToString());
			}

			if (request.VehicleIds?.Any() == true)
			{
				foreach (int vehicleId in request.VehicleIds)
					formData.Append("vehicle_ids", vehicleId.ToString());
			}

			if (request.AlarmcodeId.HasValue)
				formData.Append("alarmcode_id", request.AlarmcodeId.ToString());

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this SetAlarmConfirmationRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.AlarmId.HasValue)
				throw new ArgumentNullException(nameof(request.AlarmId), "The alarm id is required");

			if (!request.StatusId.HasValue)
				throw new ArgumentNullException(nameof(request.StatusId), "The status id is required");

			var formData = new FormData();
			formData.Append("Confirmation[alarm_id]", request.AlarmId.ToString());
			formData.Append("Confirmation[status_id]", request.StatusId.ToString());

			if (!string.IsNullOrWhiteSpace(request.Note))
				formData.Append("Confirmation[note]", request.Note.Trim());

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this CloseAlarmRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.Id.HasValue)
				throw new ArgumentNullException(nameof(request.Id), "The alarm id is required");

			var formData = new FormData();
			formData.Append("Alarm[id]", request.Id.ToString());

			if (request.IsClosed.HasValue)
				formData.Append("Alarm[closed]", (request.IsClosed.Value ? 1 : 0).ToString());

			if (!string.IsNullOrWhiteSpace(request.Report))
				formData.Append("Alarm[report]", request.Report.Trim());

			if (request.TimestampUnix.HasValue)
				formData.Append("Alarm[ts]", request.TimestampUnix.ToString());

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this CreateMessageRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (string.IsNullOrWhiteSpace(request.Body))
				throw new ArgumentNullException(nameof(request.Body), "A message body is required");

			var formData = new FormData();
			formData.Append("text", request.Body.Trim());

			if (!string.IsNullOrWhiteSpace(request.Number))
				formData.Append("number", request.Number.Trim());

			if (!string.IsNullOrWhiteSpace(request.Title))
				formData.Append("title", request.Title.Trim());

			if (!string.IsNullOrWhiteSpace(request.Address))
				formData.Append("address", request.Address.Trim());

			if (request.Rics?.Any() == true)
				formData.Append("ric", string.Join(",", request.Rics));

			if (request.Persons?.Any() == true)
				formData.Append("person", string.Join(",", request.Persons));

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this MarkViewedRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.Id.HasValue)
				throw new ArgumentNullException(nameof(request.Id), "A dataset id is required");

			if (!request.Type.HasValue)
				throw new ArgumentNullException(nameof(request.Type), "A dataset type is required");

			var formData = new FormData();
			formData.Append("id", request.Id.ToString());
			formData.Append("type", request.Type.ToString());

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this ConfirmEventRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.EventId.HasValue)
				throw new ArgumentNullException(nameof(request.EventId), "A event id is required");

			if (request.Type == EventConfirmType.Undefined)
				throw new ArgumentNullException(nameof(request.Type), "A confirm type is required");

			if (!Enum.GetValues(typeof(EventConfirmType)).Cast<EventConfirmType>().Contains(request.Type))
				throw new ArgumentException(nameof(request.Type), "The provided type is unkown");

			var formData = new FormData();
			formData.Append("Confirmation[event_id]", request.EventId.ToString());
			formData.Append("Confirmation[type]", ((int)request.Type).ToString());

			if (!string.IsNullOrWhiteSpace(request.Message))
				formData.Append("Confirmation[custom_answer]", request.Message.Trim());

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this ConfirmSurveyRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.SurveyId.HasValue)
				throw new ArgumentNullException(nameof(request.SurveyId), "A survey id is required");

			if (!request.AnswerId.HasValue)
				throw new ArgumentNullException(nameof(request.AnswerId), "An answer id is required");

			if (!request.IsChecked.HasValue)
				throw new ArgumentNullException(nameof(request.IsChecked), "A checked status is required");

			if (request.AnswerId == 0 && string.IsNullOrWhiteSpace(request.Message))
				throw new ArgumentNullException(nameof(request.Message), "A message is required when the answer id is zero (0)");

			var formData = new FormData();
			formData.Append("Survey[news_survey_id]", request.SurveyId.ToString());
			formData.Append("Survey[news_survey_answer_id]", request.AnswerId.ToString());
			formData.Append("Survey[checked]", request.IsChecked.ToString().ToLower());

			if (!string.IsNullOrWhiteSpace(request.Message))
				formData.Append("Survey[custom_answer]", request.Message.Trim());

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this ConvertCoordinatesRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.CoordinateX.HasValue)
				throw new ArgumentNullException(nameof(request.CoordinateX), "The x-coordinate is required");

			if (!request.CoordinateY.HasValue)
				throw new ArgumentNullException(nameof(request.CoordinateX), "The y-coordinate is required");

			var formData = new FormData();
			formData.Append("coordinates-x", request.CoordinateX?.ToString(CultureInfo.InvariantCulture));
			formData.Append("coordinates-y", request.CoordinateY?.ToString(CultureInfo.InvariantCulture));

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this PushToNaviRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.AlarmId.HasValue)
				throw new ArgumentNullException(nameof(request.AlarmId), "The alarm id is required");

			var formData = new FormData();
			formData.Append("alarm_id", request.AlarmId.ToString());

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this SetVehicleStatusRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.Status.HasValue && !request.StatusId.HasValue && string.IsNullOrWhiteSpace(request.Message))
				throw new ArgumentNullException($"Either one of the parameters {nameof(request.Status)}, {nameof(request.StatusId)} or {nameof(request.Message)} is required");

			if (string.IsNullOrWhiteSpace(request.Ric) && string.IsNullOrWhiteSpace(request.Name) && string.IsNullOrWhiteSpace(request.Issi) && string.IsNullOrWhiteSpace(request.Id))
				throw new ArgumentNullException($"Either one of the parameters {nameof(request.Ric)}, {nameof(request.Name)}, {nameof(request.Issi)} or {nameof(request.Id)} is required");

			if (request.Status.HasValue && request.StatusId.HasValue)
				throw new ArgumentException($"Only one parameter is allowed: {nameof(request.Status)} or {nameof(request.StatusId)}");

			var formData = new FormData();

			if (request.Status.HasValue)
				formData.Append("status", request.Status.ToString());

			if (request.StatusId.HasValue)
				formData.Append("status_id", request.StatusId.ToString());

			if (!string.IsNullOrWhiteSpace(request.Message))
				formData.Append("status_note", request.Message.Trim());

			if (request.Latitude.HasValue)
				formData.Append("lat", request.Latitude?.ToString(CultureInfo.InvariantCulture));

			if (request.Longitude.HasValue)
				formData.Append("lng", request.Longitude?.ToString(CultureInfo.InvariantCulture));

			if (!string.IsNullOrWhiteSpace(request.Ric))
				formData.Append("vehicle_ric", request.Ric.Trim());

			if (!string.IsNullOrWhiteSpace(request.Name))
				formData.Append("vehicle_name", request.Name.Trim());

			if (!string.IsNullOrWhiteSpace(request.Issi))
				formData.Append("vehicle_issi", request.Issi.Trim());

			if (!string.IsNullOrWhiteSpace(request.Id))
				formData.Append("vehicle_id", request.Id.Trim());

			return formData.ToHttpContent();
		}

		public static HttpContent ConvertToHttpContent(this SetUserStatusRequest request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request), "The request is missing");

			if (!request.StatusId.HasValue && !request.StatusKey.HasValue)
				throw new ArgumentNullException($"Either {nameof(request.StatusId)} or {nameof(request.StatusKey)} is required");

			if (request.StatusId.HasValue && request.StatusKey.HasValue)
				throw new ArgumentException($"Only one parameter is allowed: {nameof(request.StatusId)} or {nameof(request.StatusKey)}");

			var formData = new FormData();

			if (!string.IsNullOrWhiteSpace(request.PersonForeignKey))
				formData.Append("person", request.PersonForeignKey.Trim());

			if (request.StatusId.HasValue)
				formData.Append("Status[id]", request.StatusId.ToString());

			if (request.StatusKey.HasValue)
				formData.Append("Status[key]", request.StatusKey.ToString());

			if (request.VehicleId.HasValue)
				formData.Append("Status[vehicle]", request.VehicleId.ToString());

			if (!string.IsNullOrWhiteSpace(request.Message))
				formData.Append("Status[note]", request.Message.Trim());

			if (request.ResetDate.HasValue)
				formData.Append("Status[reset_date]", ((int)(request.ResetDate.Value - DateTimeHelper.UnixEpoch).TotalSeconds).ToString());

			if (request.ResetId.HasValue)
				formData.Append("Status[reset_to]", request.ResetId.ToString());

			if (request.SkipStatusPlan.HasValue)
				formData.Append("Status[status_skip_statusplan]", request.SkipStatusPlan.ToString().ToLower());

			if (request.SkipGeoFence.HasValue)
				formData.Append("Status[status_skip_geofence]", request.SkipGeoFence.ToString().ToLower());

			if (request.IsAlarm.HasValue)
				formData.Append("Status[alarm]", request.IsAlarm.ToString().ToLower());

			if (request.AlarmId.HasValue)
				formData.Append("Status[alarm_id]", request.AlarmId.ToString());

			return formData.ToHttpContent();
		}
	}
}
