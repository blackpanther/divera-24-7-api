﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace AMWD.Net.Api.Divera.Utils
{
	internal class FormData
	{
		private readonly List<KeyValuePair<string, string>> formData = new();

		public void Clear()
			=> formData.Clear();

		public void Append(string key, string value)
			=> formData.Add(new KeyValuePair<string, string>(key, value));

		public bool ContainsKey(string key)
			=> formData.Where(kvp => kvp.Key == key).Any();

		public HttpContent ToHttpContent()
		{
			var content = new FormUrlEncodedContent(formData);
			return content;
		}
	}
}
