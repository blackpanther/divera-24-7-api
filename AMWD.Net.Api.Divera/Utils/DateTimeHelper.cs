﻿using System;

namespace AMWD.Net.Api.Divera.Utils
{
	internal class DateTimeHelper
	{
		public static readonly DateTime UnixEpoch = new(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
	}
}
