﻿using AMWD.Net.Api.Divera.Enums;

namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to confirm an event.
	/// </summary>
	public class ConfirmEventRequest
	{
		/// <summary>
		/// Gets or sets the event id.
		/// [ID des neuen Termins]
		/// </summary>
		public int? EventId { get; set; }

		/// <summary>
		/// Gets or sets the event type.
		/// [Rückmeldung (1 = Ja, 2 = Unsicher, 3 = Nein)]
		/// </summary>
		public EventConfirmType Type { get; set; }

		/// <summary>
		/// Gets or sets a custom confirmation message.
		/// </summary>
		public string Message { get; set; }
	}
}
