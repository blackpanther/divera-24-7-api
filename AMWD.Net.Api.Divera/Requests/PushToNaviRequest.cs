﻿namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to push the address of an alarm to the navigation system.
	/// </summary>
	public class PushToNaviRequest
	{
		/// <summary>
		/// Gets or sets the alarm id.
		/// </summary>
		public int? AlarmId { get; set; }
	}
}
