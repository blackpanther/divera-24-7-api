﻿using System.Collections.Generic;

namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to create a new information message (news).
	/// </summary>
	public class CreateMessageRequest
	{
		/// <summary>
		/// Gets or sets the foreign key to a message.
		/// [Fremdschlüssel der Mitteilung]
		/// </summary>
		public string Number { get; set; }

		/// <summary>
		/// Gets or sets a title.
		/// [Titel der Mitteilung]
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the message body (required).
		/// [Text der Mitteilung]
		/// </summary>
		public string Body { get; set; }

		/// <summary>
		/// Gets or sets an address.
		/// [Adresse der Mitteilung]
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// Gets or sets the RIC(s) to inform.
		/// [Gruppe(n) gemäß dem Feld Alarmierungs-RIC]
		/// </summary>
		public ICollection<string> Rics { get; set; } = new List<string>();

		/// <summary>
		/// Gets or sets the persons to inform.
		/// [Person(en) gemäß dem Feld Fremdschlüssel]
		/// </summary>
		public ICollection<string> Persons { get; set; } = new List<string>();
	}
}
