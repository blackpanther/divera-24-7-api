﻿namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to set a vehicle status.
	/// </summary>
	public class SetVehicleStatusRequest
	{
		/// <summary>
		/// Gets or sets the vehicle status by the native FMS system. EXCLUSIVE TO <see cref="StatusId"/>.
		/// [Reguläre Nummer des Status 0 bis 9 entsprechen dem bekannten Status]
		/// </summary>
		/// <remarks>
		/// <list type="bullet">
		/// <item>0 = emergency</item>
		/// <item>1 = free on radio</item>
		/// <item>2 = free on base</item>
		/// <item>3 = on way to location</item>
		/// <item>4 = on location</item>
		/// <item>5 = request permission to speek</item>
		/// <item>6 = out of order</item>
		/// <item>7 = on way to destination</item>
		/// <item>8 = on destination</item>
		/// <item>9 = region specific status</item>
		/// </list>
		/// </remarks>
		public int? Status { get; set; }

		/// <summary>
		/// Gets or sets the vehicle status by ID. EXCLUSIVE TO <see cref="Status"/>.
		/// </summary>
		/// <remarks>
		/// <list type="bullet">
		/// <item>1 = free on radio</item>
		/// <item>2 = free on base</item>
		/// <item>3 = on way to location</item>
		/// <item>4 = on location</item>
		/// <item>5 = request permission to speek</item>
		/// <item>6 = out of order</item>
		/// <item>7 = on way to destination</item>
		/// <item>8 = on destination</item>
		/// <item>9 = region specific status</item>
		/// <item>10 = emergency</item>
		/// </list>
		/// </remarks>
		public int? StatusId { get; set; }

		/// <summary>
		/// Gets or sets a message.
		/// [Freitext-Rückmeldung]
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the latitude.
		/// [Breitengrad]
		/// </summary>
		public float? Latitude { get; set; }

		/// <summary>
		/// Gets or sets the longitude.
		/// [Längengrad]
		/// </summary>
		public float? Longitude { get; set; }

		/// <summary>
		/// Gets or sets the VEHICLE RIC.
		/// [Alarmierungs-RIC über die das Fahrzeug angesprochen wird]
		/// </summary>
		public string Ric { get; set; }

		/// <summary>
		/// Gets or sets the name (call sign).
		/// [Identifizierung des Fahrzeugs anhand des Feldes Funkrufname]
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the ISSI.
		/// [Identifizierung des Fahrzeugs anhand des Felds ISSI]
		/// </summary>
		public string Issi { get; set; }

		/// <summary>
		/// Gets or sets the id.
		/// [Identifizierung des Fahrzeugs anhand der ID]
		/// </summary>
		public string Id { get; set; }
	}
}
