﻿using System;
using AMWD.Net.Api.Divera.Utils;

namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to close an alarm.
	/// </summary>
	public class CloseAlarmRequest
	{
		/// <summary>
		/// Gets or sets the alarm id.
		/// [ID des Einsatzes]
		/// </summary>
		public int? Id { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the alarm should be closed.
		/// [Zustand geschlossen oder geöffnet]
		/// </summary>
		public bool? IsClosed { get; set; }

		/// <summary>
		/// Gets or sets a report.
		/// [Einsatzbericht]
		/// </summary>
		public string Report { get; set; }

		/// <summary>
		/// Gets or sets the timestamp as unix.
		/// [Datum/Uhrzeit]
		/// </summary>
		public int? TimestampUnix { get; set; }

		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		public DateTime? Timestamp
		{
			get => TimestampUnix.HasValue ? DateTimeHelper.UnixEpoch.AddSeconds(TimestampUnix.Value) : null;
			set => TimestampUnix = value.HasValue ? (int)value.Value.Subtract(DateTimeHelper.UnixEpoch).TotalSeconds : null;
		}
	}
}
