﻿using System;
using System.Threading;

namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to set a user status.
	/// </summary>
	public class SetUserStatusRequest
	{
		/// <summary>
		/// Gets or sets the persons foreign key (only possible with system access key).
		/// [Person gemäß dem Feld Fremdschlüssel]
		/// </summary>
		public string PersonForeignKey { get; set; }

		/// <summary>
		/// Gets or sets the status id. EXCLUSIVE TO <see cref="StatusKey"/>.
		/// [ID des neuen Status]
		/// </summary>
		public int? StatusId { get; set; }

		/// <summary>
		/// Gets or sets the status key. (rolling number) EXCLUSIVE TO <see cref="StatusId"/>.
		/// [Laufende Nummer des neuen Status]
		/// </summary>
		public int? StatusKey { get; set; }

		/// <summary>
		/// Gets or sets the vehicle id for the vehicle, the user should be mapped to.
		/// Mapping is removed on next <see cref="DiveraApi.SetUserStatus(SetUserStatusRequest, CancellationToken)"/> call.
		/// [ID des Fahrzeugs, dem der Nutzer zugewiesen werden soll]
		/// </summary>
		public int? VehicleId { get; set; }

		/// <summary>
		/// Gets or sets a message (note).
		/// Is removed on next <see cref="DiveraApi.SetUserStatus(SetUserStatusRequest, CancellationToken)"/> call.
		/// [Freitext zur Statusmeldung]
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the time, when the status should be resetted.
		/// [Zeitpunkt, an dem der Status zurückgesetzt werden soll]
		/// </summary>
		public DateTime? ResetDate { get; set; }

		/// <summary>
		/// Gets or sets the status id, to which the user should be resetted.
		/// [ID des Folgestatus bei Zurücksetzen]
		/// </summary>
		public int? ResetId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the status plan should be overriden (until <see cref="ResetDate"/> or next call of <see cref="DiveraApi.SetUserStatus(SetUserStatusRequest, CancellationToken)"/>).
		/// [Alle Termine im Zeitraum überschreiben]
		/// </summary>
		public bool? SkipStatusPlan { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to ignore all geo-fences (until <see cref="ResetDate"/> or next call of <see cref="DiveraApi.SetUserStatus(SetUserStatusRequest, CancellationToken)"/>).
		/// [Alle Geofences im Zeitraum ignorieren]
		/// </summary>
		public bool? SkipGeoFence { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether it's an alarm response.
		/// [Als Einsatzrückmeldung, bei Verwendung von <see cref="StatusKey"/> bezieht sich die laufende Nummer auf die Status-Reihenfolge gemäß der Sortierung]
		/// </summary>
		public bool? IsAlarm { get; set; }

		/// <summary>
		/// Gets or sets the corresponding alarm id. When missing, the latest alarm is selected.
		/// [ID der Alarmierung, um explizit auf einen bestimmten Einsatz zu reagieren, andernfalls erfolgt die Rückmeldung automatisch für den aktuellen Alarm]
		/// </summary>
		public int? AlarmId { get; set; }
	}
}
