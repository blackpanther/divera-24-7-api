﻿namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to convert coordinates.
	/// </summary>
	public class ConvertCoordinatesRequest
	{
		/// <summary>
		/// Gets or sets the x value.
		/// [Rechtswert]
		/// </summary>
		public float? CoordinateX { get; set; }

		/// <summary>
		/// Gets or sets the y value.
		/// [Hochwert]
		/// </summary>
		public float? CoordinateY { get; set; }
	}
}
