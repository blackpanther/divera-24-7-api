﻿namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to mark a dataset (alarm, message, appointment) as viewed/read.
	/// </summary>
	public class MarkViewedRequest
	{
		/// <summary>
		/// Gets or sets the id of the dataset.
		/// [ID des Datensatzes]
		/// </summary>
		public int? Id { get; set; }

		/// <summary>
		/// Gets or sets the type of the dataset.
		/// [Typ des Datensatzes (alarm/news/event)]
		/// </summary>
		public int? Type { get; set; }
	}
}
