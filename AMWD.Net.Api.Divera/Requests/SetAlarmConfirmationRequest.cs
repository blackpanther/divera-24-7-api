﻿namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to create a status confirm on an alarm.
	/// </summary>
	public class SetAlarmConfirmationRequest
	{
		/// <summary>
		/// The alarm id.
		/// [ID des Einsatzes]
		/// </summary>
		public int? AlarmId { get; set; }

		/// <summary>
		/// The status id.
		/// [ID des Status, der gesetzt werden soll]
		/// </summary>
		public int? StatusId { get; set; }

		/// <summary>
		/// A response note.
		/// [Freitext Rückmeldung]
		/// </summary>
		public string Note { get; set; }
	}
}
