﻿using System.Collections.Generic;

namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to create an alarm.
	/// </summary>
	public class CreateAlarmRequest
	{
		/// <summary>
		/// Gets or sets the alarm number.
		/// [Einsatznummer]
		/// </summary>
		public string Number { get; set; }

		/// <summary>
		/// Gets or sets the title (keyword).
		/// [Alarm-Stichwort]
		/// </summary>
		/// <remarks>
		/// Max. 50 chars, free version: max. 30 chars.
		/// </remarks>
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to use priority signals.
		/// [Priorität/Sonderrechte]
		/// </summary>
		public bool? HasPriority { get; set; }

		/// <summary>
		/// Gets or sets the alarm message.
		/// [Meldung der Alarmierung]
		/// </summary>
		/// <remarks>
		/// Max. 64_000 chars.
		/// </remarks>
		public string Text { get; set; }

		/// <summary>
		/// Gets or sets the target address.
		/// [Adresse des Einsatzortes]
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// Gets or sets the latitude.
		/// [Breitengrad]
		/// </summary>
		public float? Latitude { get; set; }

		/// <summary>
		/// Gets or sets the longitude.
		/// [Längengrad]
		/// </summary>
		public float? Longitude { get; set; }

		/// <summary>
		/// Gets or sets additional information of the target object.
		/// [Angaben zum Objekt/Gebäude]
		/// </summary>
		public string SceneObject { get; set; }

		/// <summary>
		/// Gets or sets information about the caller.
		/// [Angaben zur meldenden Personen]
		/// </summary>
		public string Caller { get; set; }

		/// <summary>
		/// Gets or sets information about the sick person.
		/// [Angaben zum Patienten]
		/// </summary>
		public string Patient { get; set; }

		/// <summary>
		/// Gets or sets additional information about the alarm itself.
		/// [Bemerkungen/Hinweise für die Einsatzkräfte]
		/// </summary>
		public string Remark { get; set; }

		/// <summary>
		/// Gets or sets involved units (informational).
		/// [Beteiligte Einsatzmittel]
		/// </summary>
		public string Units { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether a destination is defined.
		/// [Gibt es ein Fahrtziel?]
		/// </summary>
		public bool? HasDestination { get; set; }

		/// <summary>
		/// Gets or sets the destination address.
		/// [Adresse des Fahrtziels]
		/// </summary>
		public string DestinationAddress { get; set; }

		/// <summary>
		/// Gets or sets the destination latitude.
		/// [Breitengrad des Fahrtziels]
		/// </summary>
		public float? DestinationLatitude { get; set; }

		/// <summary>
		/// Gets or sets the destination longitude.
		/// [Längengrad des Fahrtziels]
		/// </summary>
		public float? DestinationLongitude { get; set; }

		/// <summary>
		/// Gets or sets an additional text. (First row)
		/// [Zusatz 1]
		/// </summary>
		public string AdditionalText1 { get; set; }

		/// <summary>
		/// Gets or sets an additional text. (Second row)
		/// [Zusatz 2]
		/// </summary>
		public string AdditionalText2 { get; set; }

		/// <summary>
		/// Gets or sets an additional text. (Third row)
		/// [Zusatz 3]
		/// </summary>
		public string AdditionalText3 { get; set; }

		/// <summary>
		/// Gets a manipulatable list of rics to send the alarm to.
		/// [Alarmierte Gruppen(n) oder Untereinheiten]
		/// </summary>
		/// <remarks>
		/// The name has to be configured first on the groups via web interface.
		/// </remarks>
		public ICollection<string> Rics { get; set; } = new List<string>();

		/// <summary>
		/// Gets a manipulatable list of vehicles to send the alarm to.
		/// [Alarmierte Fahrzeug(e)]
		/// </summary>
		public ICollection<string> Vehicles { get; set; } = new List<string>();

		/// <summary>
		/// Gets a manipulatable list of persons to send the alarm to.
		/// [Alarmierte Person(en)]
		/// </summary>
		public ICollection<string> Persons { get; set; } = new List<string>();

		/// <summary>
		/// Gets or sets the delay in seconds since the alarm was captured.
		/// [Sekunden, die seit dem Alarm vergangen sind]
		/// </summary>
		public int? DelaySeconds { get; set; }

		/// <summary>
		/// Gets or sets a different coordinate system to use.
		/// [Aktiviert ein abweichendes Koordinatensystem]
		/// </summary>
		public string Coordinates { get; set; }

		/// <summary>
		/// Gets or sets the x value of the coordinate system.
		/// [Rechtswert einer Koordinate]
		/// </summary>
		public float? CoordinatesX { get; set; }

		/// <summary>
		/// Gets or sets the y value of the coordinate system.
		/// [Hochwert einer Koordinate]
		/// </summary>
		public float? CoordinatesY { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to create a news instead of an alarm.
		/// [Mitteilung statt einer Alarmierung erstellen]
		/// </summary>
		public bool? IsNews { get; set; }

		/// <summary>
		/// Gets or sets the ID of an already existing alarm.
		/// [Primärschlüssel einer bestehenden Alarmierung]
		/// </summary>
		public int? Id { get; set; }

		/// <summary>
		/// Gets or sets a list of primary keys of clusters (sub-units).
		/// [Primärschlüssel von Untereinheiten]
		/// </summary>
		public ICollection<int> ClusterIds { get; set; }

		/// <summary>
		/// Gets or sets a list of primary keys of groups.
		/// [Primärschlüssel von Gruppen]
		/// </summary>
		public ICollection<int> GroupIds { get; set; }

		/// <summary>
		/// Gets or sets a list of primary keys of users.
		/// [Primärschlüssel von Benutzern]
		/// </summary>
		public ICollection<int> UserClusterRelationIds { get; set; }

		/// <summary>
		/// Gets or sets a list of primary keys of vehicles.
		/// [Primärschlüssel von Einsatzfahrzeugen]
		/// </summary>
		public ICollection<int> VehicleIds { get; set; }

		/// <summary>
		/// Gets or sets primary key of an alarm template.
		/// [Primärschlüssel einer Alarmvorlage]
		/// </summary>
		public int? AlarmcodeId { get; set; }
	}
}
