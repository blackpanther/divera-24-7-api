﻿namespace AMWD.Net.Api.Divera.Requests
{
	/// <summary>
	/// The request model to confirm a survey choice.
	/// </summary>
	public class ConfirmSurveyRequest
	{
		/// <summary>
		/// Gets or sets the survey id.
		/// [ID der Umfrage]
		/// </summary>
		public int? SurveyId { get; set; }

		/// <summary>
		/// Gets or sets the answer id.
		/// [ID der Antwort]
		/// </summary>
		public int? AnswerId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the answer is checked.
		/// [Zustand der Antwort]
		/// </summary>
		public bool? IsChecked { get; set; }

		/// <summary>
		/// Gets or sets a custom answer.
		/// [Freitext]
		/// </summary>
		/// <remarks>
		/// To use this answer, the <see cref="AnswerId"/> has to be zero (0).
		/// </remarks>
		public string Message { get; set; }
	}
}
