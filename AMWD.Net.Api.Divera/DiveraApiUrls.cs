﻿namespace AMWD.Net.Api.Divera
{
	/// <summary>
	/// Listing of API endpoints.
	/// </summary>
	public static class DiveraApiUrls
	{
		/// <summary>
		/// The base url for all API requests.
		/// </summary>
		public static readonly string Base = "https://www.divera247.com/api";

		/// <summary>
		/// The url to create a new alarm.
		/// </summary>
		public static readonly string CreateAlarm = $"{Base}/alarm";
		/// <summary>
		/// The url to retrieve information of the latest (last) alarm.
		/// </summary>
		public static readonly string GetLatestAlarm = $"{Base}/last-alarm";
		/// <summary>
		/// The url to confirm an alarm with an status.
		/// </summary>
		public static readonly string ConfirmAlarm = $"{Base}/confirm-alarm";
		/// <summary>
		/// The url to close an alarm.
		/// </summary>
		public static readonly string CloseAlarm = $"{Base}/close-alarm";

		/// <summary>
		/// The url to create a news entry.
		/// </summary>
		public static readonly string CreateNews = $"{Base}/news";
		/// <summary>
		/// The url to mark a dataset as viewed.
		/// </summary>
		public static readonly string SetViewed = $"{Base}/viewed";
		/// <summary>
		/// The url to confirm an event.
		/// </summary>
		public static readonly string ConfirmEvent = $"{Base}/confirm-event";
		/// <summary>
		/// The url to confirm a survey.
		/// </summary>
		public static readonly string ConfirmSurvey = $"{Base}/confirm-survey";

		/// <summary>
		/// The rul to conver DHDN to UTM coordinates.
		/// </summary>
		public static readonly string ConvertCoordinates = $"{Base}/convert-dhdn-to-wgs84";
		/// <summary>
		/// The url to push an address to the navigation system (TETRAcontrol NBX)
		/// </summary>
		public static readonly string PushAddress = $"{Base}/push-address-to-navi";

		/// <summary>
		/// The url to set a vehicle status (FMS).
		/// </summary>
		public static readonly string SetVehicleStatus = $"{Base}/fms";

		/// <summary>
		/// The url to set a user status (available, etc.).
		/// </summary>
		public static readonly string SetUserStatus = $"{Base}/setstatus";

		/// <summary>
		/// The url toe get a list of users.
		/// </summary>
		public static readonly string GetUsers = $"{Base}/users";

		/// <summary>
		/// The url to trigger a push test.
		/// </summary>
		public static readonly string TestPush = $"{Base}/test-push";
	}
}
