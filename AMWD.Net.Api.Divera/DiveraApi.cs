﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.Divera.Exceptions;
using AMWD.Net.Api.Divera.Models;
using AMWD.Net.Api.Divera.Requests;
using AMWD.Net.Api.Divera.Utils;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera
{
	/// <summary>
	/// Implements the Divera 24/7 API. (www.divera247.com)
	/// </summary>
	/// <seealso cref="System.IDisposable" />
	public class DiveraApi : IDisposable
	{
		private readonly HttpClient httpClient;

		/// <summary>
		/// Initializes a new instance of the <see cref="DiveraApi"/> class.
		/// </summary>
		public DiveraApi()
		{
			httpClient = new HttpClient();
		}

		/// <summary>
		/// Gets or sets the access key.
		/// </summary>
		public string AccessKey { get; set; }

		/// <summary>
		/// Creates a new alarm.
		/// [Methode zum Alarmieren von Einheiten]
		/// </summary>
		/// <param name="request">The alarm request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request is missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> CreateAlarm(CreateAlarmRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.CreateAlarm, requestContent, cancellationToken);
		}

		/// <summary>
		/// Gets the information of the latest alarm.
		/// [Abfrage von Informationen zum letzten Einsatz]
		/// </summary>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>The alarm information or <c>null</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public async Task<AlarmResult> GetLatestAlarm(CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var response = await httpClient.GetAsync($"{DiveraApiUrls.GetLatestAlarm}?accesskey={AccessKey}", cancellationToken);
			string body = await response.Content.ReadAsStringAsync();
			var successResponse = JsonConvert.DeserializeObject<Success>(body);

			if (!response.IsSuccessStatusCode)
			{
				if (response.StatusCode == HttpStatusCode.Forbidden)
					throw new AccessKeyException("The access key is invalid");

				throw new DiveraException($"HTTP error ({(int)response.StatusCode}): {successResponse?.ErrorMessage}");
			}

			return JsonConvert.DeserializeObject<AlarmResult>(body);
		}

		/// <summary>
		/// Sends a confirmation to a running alarm.
		/// [Rückmeldung eines offenen Einsatzes]
		/// </summary>
		/// <param name="request">The confirmation request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required arguments are missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> SetAlarmConfirmation(SetAlarmConfirmationRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.ConfirmAlarm, requestContent, cancellationToken);
		}

		/// <summary>
		/// Closes a running alarm or adds a report.
		/// [Alarm schließen / Einsatzbericht eintragen]
		/// </summary>
		/// <param name="request">The close request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request is missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> CloseAlarm(CloseAlarmRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.CloseAlarm, requestContent, cancellationToken);
		}

		/// <summary>
		/// Creates a new message.
		/// [Erstellen von Mitteilungen]
		/// </summary>
		/// <param name="request">The create request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required parameters are missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> CreateMessage(CreateMessageRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.CreateNews, requestContent, cancellationToken);
		}

		/// <summary>
		/// Marks a dataset (alarm, message, appointment) as viewed/read.
		/// [Datensatz als gelesen markieren]
		/// </summary>
		/// <param name="request">The mark request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request is missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> MarkViewed(MarkViewedRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.SetViewed, requestContent, cancellationToken);
		}

		/// <summary>
		/// Confirms an event with a response.
		/// </summary>
		/// <param name="request">The confirmation request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required parameters are missing.</exception>
		/// <exception cref="ArgumentException">The parameters have invalid values.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> ConfirmEvent(ConfirmEventRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.ConfirmEvent, requestContent, cancellationToken);
		}

		/// <summary>
		/// Confirms a survey.
		/// </summary>
		/// <param name="request">The confirmation request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required parameters are missing.</exception>
		/// <exception cref="ArgumentException">The parameters have invalid values.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> ConfirmSurvey(ConfirmSurveyRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.ConfirmSurvey, requestContent, cancellationToken);
		}

		/// <summary>
		/// Converts coordinates to an address.
		/// [Umrechnung von Gauß-Krüger (Deutschen Hauptdreiecksnetz, DHDN) in UTM-Koordinaten]
		/// </summary>
		/// <param name="request">The conversion request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>The converted address.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required parameters are missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public async Task<string> ConvertCoordinates(ConvertCoordinatesRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			var response = await SendRequestWithDataResponse<ConvertedAddress>(DiveraApiUrls.ConvertCoordinates, requestContent, cancellationToken);
			return response.Address;
		}

		/// <summary>
		/// Pushs the address of an alarm to the navigation system (using TETRAcontrol NBX).
		/// [Einsatz zum Navigationsgerät über TETRAcontrol NBX übertragen]
		/// </summary>
		/// <param name="request">The push request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required parameters are missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> PushToNavi(PushToNaviRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.PushAddress, requestContent, cancellationToken);
		}

		/// <summary>
		/// Sets the vehicle status.
		/// [Setzen des Fahrzeugstatus (FMS)]
		/// </summary>
		/// <param name="request">The status set request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentException">The parameters are invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required parameters are missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<bool> SetVehicleStatus(SetVehicleStatusRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDefaultResponse(DiveraApiUrls.SetVehicleStatus, requestContent, cancellationToken);
		}

		/// <summary>
		/// Sets the user status.
		/// [Setzen des Benutzerstatus]
		/// </summary>
		/// <param name="request">The status request.</param>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>The status data to confirm otherwise <c>null</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="ArgumentException">The parameters are invalid.</exception>
		/// <exception cref="ArgumentNullException">The request or required parameters are missing.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public Task<SetStatusData> SetUserStatus(SetUserStatusRequest request, CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var requestContent = request.ConvertToHttpContent();
			return SendRequestWithDataResponse<SetStatusData>(DiveraApiUrls.SetUserStatus, requestContent, cancellationToken);
		}

		/// <summary>
		/// Gets the user list.
		/// [Auflistung aller Nutzer einer Einheit, ihrer Fremdschlüssel (falls vorhanden) und dem aktuellen Status]
		/// </summary>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns>The list of users or <c>null</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public async Task<List<User>> GetUsers(CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var response = await httpClient.GetAsync($"{DiveraApiUrls.GetUsers}?accesskey={AccessKey}", cancellationToken);
			string body = await response.Content.ReadAsStringAsync();
			var content = JsonConvert.DeserializeObject<Success<List<User>>>(body);

			if (!response.IsSuccessStatusCode)
			{
				if (response.StatusCode == HttpStatusCode.Forbidden)
					throw new AccessKeyException("The access key is invalid");

				throw new DiveraException($"HTTP error ({(int)response.StatusCode}): {content?.ErrorMessage}");
			}

			return content.Data;
		}

		/// <summary>
		/// Sends a push message to all connected devices.
		/// [Der Benutzer erhält sofort eine Push-Nachricht an alle Endgeräte mit aktivem Login]
		/// </summary>
		/// <param name="cancellationToken">A token to cancel the method call.</param>
		/// <returns><c>true</c> on success, otherwise <c>false</c>.</returns>
		/// <exception cref="AccessKeyException">The access key is missing or invalid.</exception>
		/// <exception cref="DiveraException">The response from Divera was an unexpected error.</exception>
		public async Task<bool> TestPush(CancellationToken cancellationToken = default)
		{
			CheckAccessKeyOrDisposed();

			var response = await httpClient.PostAsync($"{DiveraApiUrls.TestPush}?accesskey={AccessKey}", null, cancellationToken);
			string body = await response.Content.ReadAsStringAsync();
			var content = JsonConvert.DeserializeObject<Success>(body);

			if (!response.IsSuccessStatusCode)
			{
				if (response.StatusCode == HttpStatusCode.Forbidden)
					throw new AccessKeyException("The access key is invalid");

				throw new DiveraException($"HTTP error ({(int)response.StatusCode}): {content?.ErrorMessage}");
			}

			return content.IsSuccess;
		}

		private async Task<bool> SendRequestWithDefaultResponse(string url, HttpContent httpContent, CancellationToken cancellationToken)
		{
			var response = await httpClient.PostAsync($"{url}?accesskey={AccessKey}", httpContent, cancellationToken);
			string body = await response.Content.ReadAsStringAsync();
			var content = JsonConvert.DeserializeObject<Success>(body);

			if (!response.IsSuccessStatusCode)
			{
				if (response.StatusCode == HttpStatusCode.Forbidden)
					throw new AccessKeyException("The access key is invalid");

				throw new DiveraException($"HTTP error ({(int)response.StatusCode}): {content?.ErrorMessage}");
			}

			return content.IsSuccess;
		}

		private async Task<T> SendRequestWithDataResponse<T>(string url, HttpContent httpContent, CancellationToken cancellationToken)
		{
			var response = await httpClient.PostAsync($"{url}?accesskey={AccessKey}", httpContent, cancellationToken);
			string body = await response.Content.ReadAsStringAsync();
			var content = JsonConvert.DeserializeObject<Success<T>>(body);

			if (!response.IsSuccessStatusCode)
			{
				if (response.StatusCode == HttpStatusCode.Forbidden)
					throw new AccessKeyException("The access key is invalid");

				throw new DiveraException($"HTTP error ({(int)response.StatusCode}): {content?.ErrorMessage}");
			}

			return content.Data;
		}

		#region IDisposable implementation

		private bool isDisposed;

		/// <inheritdoc/>
		public void Dispose()
		{
			if (isDisposed)
				return;

			isDisposed = true;
			httpClient.Dispose();
		}

		private void CheckAccessKeyOrDisposed()
		{
			if (isDisposed)
				throw new ObjectDisposedException(GetType().FullName);

			if (string.IsNullOrWhiteSpace(AccessKey))
				throw new AccessKeyException("The access key is required");
		}

		#endregion IDisposable implementation
	}
}
