﻿namespace AMWD.Net.Api.Divera.Enums
{
	/// <summary>
	/// Listing of notification types.
	/// </summary>
	public enum DiveraNotificationType
	{
		/// <summary>
		/// No type.
		/// </summary>
		None = 0,
		/// <summary>
		/// Selected clusters (see other array).
		/// </summary>
		SelectedClusters = 1,
		/// <summary>
		/// All clusters.
		/// </summary>
		AllClusters = 2,
		/// <summary>
		/// Selected groups (see other array).
		/// </summary>
		SelectedGroups = 3,
		/// <summary>
		/// Selected users (see other array).
		/// </summary>
		SelectedUsers = 4
	}
}
