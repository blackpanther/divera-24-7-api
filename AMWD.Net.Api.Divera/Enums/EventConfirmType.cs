﻿namespace AMWD.Net.Api.Divera.Enums
{
	/// <summary>
	/// Listing of event confirmation types.
	/// </summary>
	public enum EventConfirmType
	{
		/// <summary>
		/// Not defined.
		/// </summary>
		Undefined = 0,
		/// <summary>
		/// Answer is yes.
		/// </summary>
		Yes = 1,
		/// <summary>
		/// Answer is 'not for sure'.
		/// </summary>
		NotSure = 2,
		/// <summary>
		/// Answer is no.
		/// </summary>
		No = 3
	}
}
