﻿using System;
using System.Collections.Generic;
using AMWD.Net.Api.Divera.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.Net.Api.Divera.Models
{
	/// <summary>
	/// The status set information as defined by Divera 24/7.
	/// </summary>
	public class SetStatusData
	{
		/// <summary>
		/// Gets or sets the status id.
		/// </summary>
		[JsonProperty("status_id")]
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to skip the status-plan.
		/// </summary>
		[JsonProperty("status_skip_statusplan")]
		public bool SkipStatusPlan { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to skip the geo-fence.
		/// </summary>
		[JsonProperty("status_skip_geofence")]
		public bool SkipGeoFence { get; set; }

		/// <summary>
		/// Gets or sets the time as unix timestamp.
		/// </summary>
		[JsonProperty("status_set_date")]
		public int SetDateUnix { get; set; }

		/// <summary>
		/// Gets or sets the time of the status (UTC).
		/// </summary>
		[JsonIgnore]
		public DateTime SetDate
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(SetDateUnix);
			set => SetDateUnix = (int)(value - DateTimeHelper.UnixEpoch).TotalSeconds;
		}

		/// <summary>
		/// Gets or set the reset time as unix timestamp.
		/// </summary>
		[JsonProperty("status_reset_date")]
		public int ResetDateUnix { get; set; }   // TODO: In example "";

		/// <summary>
		/// Gets or sets the reset time (UTC).
		/// </summary>
		[JsonIgnore]
		public DateTime ResetDate
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(ResetDateUnix);
			set => ResetDateUnix = (int)(value - DateTimeHelper.UnixEpoch).TotalSeconds;
		}

		/// <summary>
		/// Gets or sets the reset id.
		/// </summary>
		[JsonProperty("status_reset_id")]
		public int ResetId { get; set; }

		/// <summary>
		/// Gets or sets a log.
		/// </summary>
		[JsonProperty("status_log")]
		public ICollection<JObject> Log { get; set; }

		/// <summary>
		/// Gets or sets a list of changes.
		/// </summary>
		[JsonProperty("status_changes")]
		public ICollection<SetStatusChange> Changes { get; set; } = new List<SetStatusChange>();

		/// <summary>
		/// Gets or sets a note.
		/// </summary>
		[JsonProperty("note")]
		public string Note { get; set; }

		/// <summary>
		/// Gets or sets the vehicle id.
		/// </summary>
		[JsonProperty("vehicle")]
		public int VehicleId { get; set; }

		/// <summary>
		/// Gets or sets the set time as unix timestamp.
		/// </summary>
		[JsonProperty("ts")]
		public int TimestampUnix { get; set; }

		/// <summary>
		/// Gets or sets the set time (UTC).
		/// </summary>
		[JsonIgnore]
		public DateTime Timestamp
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(TimestampUnix);
			set => TimestampUnix = (int)(value - DateTimeHelper.UnixEpoch).TotalSeconds;
		}
	}
}
