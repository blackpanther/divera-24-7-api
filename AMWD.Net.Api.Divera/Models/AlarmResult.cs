﻿using System;
using System.Collections.Generic;
using AMWD.Net.Api.Divera.Enums;
using AMWD.Net.Api.Divera.Utils;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Models
{
	/// <summary>
	/// The alarm information as defined by Divera 24/7.
	/// </summary>
	public class AlarmResult
	{
		/// <summary>
		/// Gets or sets the unique id.
		/// [ID/Primärschlüssel]
		/// </summary>
		[JsonProperty("id")]
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets the foreign id.
		/// [Einsatznummer/Fremdschlüssel]
		/// </summary>
		[JsonProperty("foreign_id")]
		public string ForeignId { get; set; }

		/// <summary>
		/// Gets or sets the author id.
		/// [ID des Nutzers (UserClusterRelation)]
		/// </summary>
		[JsonProperty("author_id")]
		public int AuthorId { get; set; }

		/// <summary>
		/// Gets or sets the alarmcode id.
		/// [ID der Alarmvorlage (Alarmcode)]
		/// </summary>
		[JsonProperty("alarmcode_id")]
		public int AlarmcodeId { get; set; }

		/// <summary>
		/// Gets or sets the alarm time as unix timestamp.
		/// [Alarmierungszeit als UNIX-Timestamp]
		/// </summary>
		[JsonProperty("date")]
		public int DateUnix { get; set; }

		/// <summary>
		/// Gets or sets the alarm timestamp (UTC).
		/// </summary>
		[JsonIgnore]
		public DateTime Date
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(DateUnix);
			set { DateUnix = (int)value.Subtract(DateTimeHelper.UnixEpoch).TotalSeconds; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether to use priority signals.
		/// [Priorität/Sonderrechte]
		/// </summary>
		[JsonProperty("priority")]
		public bool HasPriority { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// [Stichwort]
		/// </summary>
		[JsonProperty("title")]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the text.
		/// [Meldung]
		/// </summary>
		[JsonProperty("text")]
		public string Text { get; set; }

		/// <summary>
		/// Gets or sets the address.
		/// [Einsatzort]
		/// </summary>
		[JsonProperty("address")]
		public string Address { get; set; }

		/// <summary>
		/// Gets or sets the latitude.
		/// [Breitengrad]
		/// </summary>
		[JsonProperty("lat")]
		public float Latitude { get; set; }

		/// <summary>
		/// Gets or sets the longitude.
		/// [Längengrad]
		/// </summary>
		[JsonProperty("lng")]
		public float Longitude { get; set; }

		/// <summary>
		/// Gets or sets the scene object.
		/// [Angaben zum Objekt/Gebäude]
		/// </summary>
		[JsonProperty("scene_object")]
		public string SceneObject { get; set; }

		/// <summary>
		/// Gets or sets the caller.
		/// [Angaben zur meldenden Personen]
		/// </summary>
		[JsonProperty("caller")]
		public string Caller { get; set; }

		/// <summary>
		/// Gets or sets the sick person.
		/// [Angaben zum Patienten]
		/// </summary>
		[JsonProperty("patient")]
		public string Patient { get; set; }

		/// <summary>
		/// Gets or sets the remark.
		/// [Bemerkungen/Hinweise für die Einsatzkräfte]
		/// </summary>
		[JsonProperty("remark")]
		public string Remark { get; set; }

		/// <summary>
		/// Gets or sets the units.
		/// [Beteiligte Einsatzmittel]
		/// </summary>
		[JsonProperty("units")]
		public string Units { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether a destination is given.
		/// [Gibt es ein Fahrtziel?]
		/// </summary>
		[JsonProperty("destination")]
		public bool HasDestination { get; set; }

		/// <summary>
		/// Gets or sets the destination address.
		/// [Adresse des Fahrtziels]
		/// </summary>
		[JsonProperty("destination_address")]
		public string DestinationAddress { get; set; }

		/// <summary>
		/// Gets or sets the destination latitude.
		/// [Breitengrad des Fahrtziels]
		/// </summary>
		[JsonProperty("destination_lat")]
		public float DestinationLatitude { get; set; }

		/// <summary>
		/// Gets or sets the destination longitude.
		/// [Längengrad des Fahrtziels]
		/// </summary>
		[JsonProperty("destination_lng")]
		public float DestinationLongitude { get; set; }

		/// <summary>
		/// Gets or sets the additional text 1.
		/// [Zusatz 1]
		/// </summary>
		[JsonProperty("additional_text_1")]
		public string AdditionalText1 { get; set; }

		/// <summary>
		/// Gets or sets the additional text 2.
		/// [Zusatz 2]
		/// </summary>
		[JsonProperty("additional_text_2")]
		public string AdditionalText2 { get; set; }

		/// <summary>
		/// Gets or sets the additional text 3.
		/// [Zusatz 3]
		/// </summary>
		[JsonProperty("additional_text_3")]
		public string AdditionalText3 { get; set; }

		/// <summary>
		/// Gets or sets the report.
		/// [Einsatzbericht]
		/// </summary>
		[JsonProperty("report")]
		public string Report { get; set; }

		/// <summary>
		/// Gets or sets the cluster ids.
		/// [IDs der Standorte (notification_type = 1)]
		/// </summary>
		[JsonProperty("cluster")]
		public List<int> ClusterIds { get; set; } = new();

		/// <summary>
		/// Gets or sets the group ids.
		/// [IDs der Gruppen (notification_type = 1|3)]
		/// </summary>
		[JsonProperty("group")]
		public List<int> GroupIds { get; set; } = new();

		/// <summary>
		/// Gets or sets the user cluster relation ids.
		/// [IDs der Benutzer (notification_type = 1|4)]
		/// </summary>
		[JsonProperty("user_cluster_relation")]
		public List<int> UserClusterRelationIds { get; set; } = new();

		/// <summary>
		/// Gets or sets the vehicle ids.
		/// [IDs der Fahrzeuge/Einsatzmittel]
		/// </summary>
		[JsonProperty("vehicle")]
		public List<int> VehicleIds { get; set; } = new();

		/// <summary>
		/// Gets or sets a value indicating whether the alarm is on private mode.
		/// [Sichtbarkeit privat]
		/// </summary>
		[JsonProperty("private_mode")]
		public bool IsPrivateMode { get; set; }

		/// <summary>
		/// Gets or sets the type of the notification.
		/// [Empfänger-Auswahl (1 = Ausgewählte Standorte, 2 = Alle des Standortes, 3 = Ausgewählte Gruppen, 4 = Ausgewählte Benutzer)]
		/// </summary>
		[JsonProperty("notification_type")]
		public DiveraNotificationType NotificationType { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to alarm only persons on vehicles.
		/// [Nur Personen alarmieren, die sich auf einem zugeteilten Fahrzeuge befinden]
		/// </summary>
		[JsonProperty("notification_filter_vehicle")]
		public bool HasNotificationFilterVehicle { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to alarm persons with a specific state.
		/// [Nur Personen alarmieren, die sich in einem bestimmten Status befinden]
		/// </summary>
		[JsonProperty("notification_filter_status")]
		public bool HasNotificationFilterStatus { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to display this alarm for other persons than only alarmed.
		/// [Einsatz auch für nicht benachrichtigte Empfänger sichtbar machen]
		/// </summary>
		[JsonProperty("notification_filter_access")]
		public bool HasNotificationFilterAccess { get; set; }

		/// <summary>
		/// Gets or sets the notification filter status ids.
		/// [IDs der ausgewählten Status (notification_filter_status = TRUE)]
		/// </summary>
		[JsonProperty("notification_filter_status_ids")]
		public List<int> NotificationFilterStatusIds { get; set; } = new();

		/// <summary>
		/// Gets or sets a value indicating whether to send push messages.
		/// [Push-Nachricht an die App senden (der Wert wird nach dem Versand auf FALSE zurückgesetzt)]
		/// </summary>
		[JsonProperty("send_push")]
		public bool DoSendPush { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to send sms messages.
		/// [SMS senden (der Wert wird nach dem Versand auf FALSE zurückgesetzt, kostenpflichtig)]
		/// </summary>
		[JsonProperty("send_sms")]
		public bool DoSendSms { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to trigger calls.
		/// [Sprachanruf senden (der Wert wird nach dem Versand auf FALSE zurückgesetzt, kostenpflichtig)]
		/// </summary>
		[JsonProperty("send_call")]
		public bool DoSendCall { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to send e-mail messages.
		/// [E-Mail senden (der Wert wird nach dem Versand auf FALSE zurückgesetzt, Adresse muss zuvor durch den Benutzer bestätigt werden)]
		/// </summary>
		[JsonProperty("send_mail")]
		public bool DoSendMail { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to trigger linked pagers.
		/// [An verknüpfte Pager senden (der Wert wird nach dem Versand auf FALSE zurückgesetzt, je nach Setup e*Message-/UNITRONIC-/TETRA-Pager)]
		/// </summary>
		[JsonProperty("send_pager")]
		public bool DoSendPager { get; set; }

		/// <summary>
		/// Gets or sets the response time in minutes.
		/// [Rückmeldung innerhalb des Zeitraums in Minuten]
		/// </summary>
		[JsonProperty("response_time")]
		public int ResponseTimeMinutes { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the alarm is closed.
		/// [Einsatz beendet]
		/// </summary>
		[JsonProperty("closed")]
		public bool IsClosed { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the alarm is new.
		/// [Neu/Umngelesen]
		/// </summary>
		[JsonProperty("new")]
		public bool IsNew { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the alarm is editable.
		/// [Bearbeitbar]
		/// </summary>
		[JsonProperty("editable")]
		public bool IsEditable { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the alarm is answerable.
		/// [Beantwortbar]
		/// </summary>
		[JsonProperty("answerable")]
		public bool IsAnswerable { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the alarm is a draft.
		/// [Entwurf]
		/// </summary>
		[JsonProperty("hidden")]
		public bool IsDraft { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the alarm is deleted.
		/// [Im Archiv, nur noch für Schreibberechtigte zugänglich]
		/// </summary>
		[JsonProperty("deleted")]
		public bool IsDeleted { get; set; }

		/// <summary>
		/// Gets or sets the user ids addressed.
		/// [Adressierte Benutzer]
		/// </summary>
		[JsonProperty("ucr_adressed")]
		public List<int> UserIdsAddressed { get; set; } = new();

		/// <summary>
		/// Gets or sets the user ids answered.
		/// [Rückmeldungen der Benutzer]
		/// </summary>
		[JsonProperty("ucr_answered")]
		public List<int> UserIdsAnswered { get; set; } = new();

		/// <summary>
		/// Gets or sets a value indicating whether the user is addressed and can respond.
		/// [Selbst adressiert, Rückmeldung ist möglich]
		/// </summary>
		[JsonProperty("ucr_self_addressed")]
		public bool IsSelfAddressed { get; set; }

		/// <summary>
		/// Gets or sets the user's status id.
		/// [Eigene Rückmeldung (ID des Status)]
		/// </summary>
		[JsonProperty("ucr_self_status_id")]
		public int SelfStatusId { get; set; }

		/// <summary>
		/// Gets or sets the user's response note.
		/// [Eigene Rückmeldung (Freitext)]
		/// </summary>
		[JsonProperty("ucr_self_note")]
		public string SelfNote { get; set; }

		/// <summary>
		/// Gets or sets the number of recipients.
		/// [Anzahl der Empfänger]
		/// </summary>
		[JsonProperty("count_recipients")]
		public int RecipientsCount { get; set; }

		/// <summary>
		/// Gets or sets the number of users read.
		/// [Anzahl gelesen]
		/// </summary>
		[JsonProperty("count_read")]
		public int ReadCount { get; set; }

		/// <summary>
		/// Gets or sets the response timestamp unix.
		/// [Berechneter UNIX-Timestamp für Rückmeldung bis (gemäß response_time)]
		/// </summary>
		[JsonProperty("ts_response")]
		public int ResponseTimestampUnix { get; set; }

		/// <summary>
		/// Gets or sets the response timestamp (UTC).
		/// </summary>
		[JsonIgnore]
		public DateTime ResponseTimestamp
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(ResponseTimestampUnix);
			set { ResponseTimestampUnix = (int)value.Subtract(DateTimeHelper.UnixEpoch).TotalSeconds; }
		}

		/// <summary>
		/// Gets or sets the publish timestamp unix.
		/// [UNIX-Timestamp für zeitgesteuerte Alarmierung]
		/// </summary>
		[JsonProperty("ts_publish")]
		public int PublishTimestampUnix { get; set; }

		/// <summary>
		/// Gets or sets the publish timestamp.
		/// </summary>
		[JsonIgnore]
		public DateTime PublishTimestamp
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(PublishTimestampUnix);
			set { PublishTimestampUnix = (int)value.Subtract(DateTimeHelper.UnixEpoch).TotalSeconds; }
		}

		/// <summary>
		/// Gets or sets the close timestamp unix.
		/// [UNIX-Timestamp Einsatzende]
		/// </summary>
		[JsonProperty("ts_close")]
		public int CloseTimestampUnix { get; set; }

		/// <summary>
		/// Gets or sets the close timestamp (UTC).
		/// </summary>
		[JsonIgnore]
		public DateTime CloseTimestamp
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(CloseTimestampUnix);
			set { CloseTimestampUnix = (int)value.Subtract(DateTimeHelper.UnixEpoch).TotalSeconds; }
		}

		/// <summary>
		/// Gets or sets the create timestamp unix.
		/// [UNIX-Timestamp des Erstelldatums]
		/// </summary>
		[JsonProperty("ts_create")]
		public int CreateTimestampUnix { get; set; }

		/// <summary>
		/// Gets or sets the create timestamp (UTC).
		/// </summary>
		[JsonIgnore]
		public DateTime CreateTimestamp
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(CreateTimestampUnix);
			set { CreateTimestampUnix = (int)value.Subtract(DateTimeHelper.UnixEpoch).TotalSeconds; }
		}

		/// <summary>
		/// Gets or sets the update timestamp unix.
		/// [UNIX-Timestamp zuletzt bearbeitet]
		/// </summary>
		[JsonProperty("ts_update")]
		public int UpdateTimestampUnix { get; set; }

		/// <summary>
		/// Gets or sets the update timestamp (UTC).
		/// </summary>
		[JsonIgnore]
		public DateTime UpdateTimestamp
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(UpdateTimestampUnix);
			set { UpdateTimestampUnix = (int)value.Subtract(DateTimeHelper.UnixEpoch).TotalSeconds; }
		}

		/// <summary>
		/// Deprecated, not used anymore. Use <see cref="HasNotificationFilterAccess"/> instead.
		/// </summary>
		[Obsolete("Use '" + nameof(HasNotificationFilterAccess) + "' instead")]
		[JsonProperty("notification_filter_status_access")]
		public bool IsNotificationFilterStatusAccess { get; set; }
	}
}
