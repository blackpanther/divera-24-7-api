﻿using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Models
{
	/// <summary>
	/// The user as defined by Divera 24/7.
	/// </summary>
	public class User
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		[JsonProperty("user_id")]
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets the user to cluster relation id.
		/// </summary>
		[JsonProperty("user_cluster_relation_id")]
		public int ClusterRelationId { get; set; }

		/// <summary>
		/// Gets or sets the cluster id.
		/// </summary>
		[JsonProperty("cluster_id")]
		public int ClusterId { get; set; }

		/// <summary>
		/// Gets or sets the status id.
		/// </summary>
		[JsonProperty("status_id")]
		public int StatusId { get; set; }

		/// <summary>
		/// Gets or sets the firstname.
		/// </summary>
		[JsonProperty("firstname")]
		public string Firstname { get; set; }

		/// <summary>
		/// Gets or sets the lastname.
		/// </summary>
		[JsonProperty("lastname")]
		public string Lastname { get; set; }

		/// <summary>
		/// Gets or sets a foreign id.
		/// </summary>
		[JsonProperty("foreign_id")]
		public string ForeignId { get; set; }
	}
}
