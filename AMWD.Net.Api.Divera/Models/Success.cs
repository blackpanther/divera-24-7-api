﻿using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Models
{
	/// <summary>
	/// The success information as defined by Divera 24/7.
	/// </summary>
	public class Success
	{
		/// <summary>
		/// Gets or sets a value indicating whether it is a success.
		/// </summary>
		[JsonProperty("success")]
		public bool IsSuccess { get; set; }

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		[JsonProperty("error")]
		public string ErrorMessage { get; set; }
	}

	/// <summary>
	/// The success information as defined by Divera24/7 with payload.
	/// </summary>
	/// <typeparam name="T">The payload type.</typeparam>
	public class Success<T> : Success
	{
		/// <summary>
		/// Gets or sets the payload.
		/// </summary>
		[JsonProperty("data")]
		public T Data { get; set; }
	}
}
