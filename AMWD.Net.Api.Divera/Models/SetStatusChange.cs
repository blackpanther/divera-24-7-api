﻿using System;
using AMWD.Net.Api.Divera.Utils;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Models
{
	/// <summary>
	/// The status change as defined by Divera 24/7.
	/// </summary>
	public class SetStatusChange
	{
		/// <summary>
		/// Gets or sets the timestamp as unix.
		/// </summary>
		[JsonProperty("ts")]
		public int TimestampUnix { get; set; }

		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		[JsonIgnore]
		public DateTime Timestamp
		{
			get => DateTimeHelper.UnixEpoch.AddSeconds(TimestampUnix);
			set => TimestampUnix = (int)(value - DateTimeHelper.UnixEpoch).TotalSeconds;
		}

		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		[JsonProperty("status")]
		public int Status { get; set; }

		/// <summary>
		/// Gets or sets a note.
		/// </summary>
		[JsonProperty("note")]
		public string Note { get; set; }

		/// <summary>
		/// Gets or sets the vehicel id.
		/// </summary>
		[JsonProperty("vehicle")]
		public int VehicleId { get; set; }

		/// <summary>
		/// Gets or sets the event id.
		/// </summary>
		[JsonProperty("event")]
		public int EventId { get; set; }

		/// <summary>
		/// Gets or sets the type.
		/// </summary>
		[JsonProperty("type")]
		public int Type { get; set; }
	}
}
