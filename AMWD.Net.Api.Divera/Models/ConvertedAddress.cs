﻿namespace AMWD.Net.Api.Divera.Models
{
	/// <summary>
	/// The coordinate to address conversion as defined by Divera 24/7.
	/// </summary>
	public class ConvertedAddress
	{
		/// <summary>
		/// Gets or sets the address resoved from coordinates.
		/// </summary>
		public string Address { get; set; }
	}
}
