﻿using System;
using System.Runtime.Serialization;

namespace AMWD.Net.Api.Divera.Exceptions
{
	/// <summary>
	/// Represents an error with the access key.
	/// </summary>
	public class AccessKeyException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AccessKeyException"/> class.
		/// </summary>
		public AccessKeyException()
			: base()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="AccessKeyException"/> class with a specified error message.
		/// </summary>
		/// <param name="message">The message that describes the error.</param>
		public AccessKeyException(string message)
			: base(message)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="AccessKeyException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception, or a <c>null</c> reference if no inner exception is specified.</param>
		public AccessKeyException(string message, Exception innerException)
			: base(message, innerException)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="AccessKeyException"/> class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="ArgumentNullException">The info parameter is null.</exception>
		/// <exception cref="SerializationException">The class name is null or <see cref="Exception.HResult"/> is zero (0).</exception>
		protected AccessKeyException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{ }
	}
}
