﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AMWD.Net.Api.Divera.Exceptions;
using AMWD.Net.Api.Divera.Models;
using AMWD.Net.Api.Divera.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class SetUserStatusTests : TestBase
	{
		[DataTestMethod]
		[DataRow(3, null)]
		[DataRow(null, 3)]
		public async Task ShouldMapFullRequest(int? statusId, int? statusKey)
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success<SetStatusData> { IsSuccess = true, Data = new SetStatusData() });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			int unixTime = (int)(date - DateTime.UnixEpoch).TotalSeconds;

			var request = new SetUserStatusRequest
			{
				PersonForeignKey = "Bob",
				StatusId = statusId,
				StatusKey = statusKey,
				VehicleId = 123,
				Message = "Hello",
				ResetDate = date,
				ResetId = 4711,
				SkipStatusPlan = false,
				SkipGeoFence = true,
				IsAlarm = true,
				AlarmId = 815
			};
			var api = GetApi(accessKey);

			// act
			var response = await api.SetUserStatus(request);

			// assert
			Assert.IsNotNull(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.SetUserStatus}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);
			var callback = HttpUtility.ParseQueryString(requestCallback.Content);

			Assert.AreEqual(request.PersonForeignKey, callback["person"]);

			if (request.StatusId.HasValue)
				Assert.AreEqual(request.StatusId, Convert<int?>(callback["Status[id]"]));

			if (request.StatusKey.HasValue)
				Assert.AreEqual(request.StatusKey, Convert<int?>(callback["Status[key]"]));

			Assert.AreEqual(request.VehicleId, Convert<int?>(callback["Status[vehicle]"]));
			Assert.AreEqual(request.Message, callback["Status[note]"]);
			Assert.AreEqual(unixTime, Convert<int?>(callback["Status[reset_date]"]));
			Assert.AreEqual(request.ResetId, Convert<int?>(callback["Status[reset_to]"]));
			Assert.AreEqual(request.SkipStatusPlan, Convert<bool?>(callback["Status[status_skip_statusplan]"]));
			Assert.AreEqual(request.SkipGeoFence, Convert<bool?>(callback["Status[status_skip_geofence]"]));
			Assert.AreEqual(request.IsAlarm, Convert<bool?>(callback["Status[alarm]"]));
			Assert.AreEqual(request.AlarmId, Convert<int?>(callback["Status[alarm_id]"]));

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingRequest()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success<SetStatusData> { IsSuccess = true, Data = new SetStatusData() });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				var response = await api.SetUserStatus(null);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingStatus()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success<SetStatusData> { IsSuccess = true, Data = new SetStatusData() });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var request = new SetUserStatusRequest();

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				var response = await api.SetUserStatus(request);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnStatusDoubleDefinition()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success<SetStatusData> { IsSuccess = true, Data = new SetStatusData() });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var request = new SetUserStatusRequest
			{
				StatusId = 21,
				StatusKey = 42
			};

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				var response = await api.SetUserStatus(request);
				Assert.Fail();
			}
			catch (ArgumentException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnInvalidAccessKey()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.Forbidden;
			var request = new SetUserStatusRequest { StatusKey = 1 };

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				var response = await api.SetUserStatus(request);
				Assert.Fail();
			}
			catch (AccessKeyException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;
			var request = new SetUserStatusRequest { StatusKey = 1 };

			var api = GetApi(accessKey);

			// act
			string exceptionMessage = null;
			try
			{
				var response = await api.SetUserStatus(request);
				Assert.Fail();
			}
			catch (DiveraException ex)
			{
				exceptionMessage = ex.Message;
			}

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.SetUserStatus}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);
			Assert.AreEqual($"HTTP error ({(int)httpResponseMessage.StatusCode}): ", exceptionMessage);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldReturnNullOnDiveraError()
		{
			// arrange
			string accessKey = "abc";
			var responseObject = new Success<SetStatusData> { IsSuccess = false, ErrorMessage = "you shall not pass" };
			string responseJson = JsonConvert.SerializeObject(responseObject);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);
			var request = new SetUserStatusRequest { StatusKey = 1 };

			var api = GetApi(accessKey);

			// act
			var response = await api.SetUserStatus(request);

			// assert
			Assert.IsNull(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.SetUserStatus}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
