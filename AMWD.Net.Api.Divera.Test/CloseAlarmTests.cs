﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AMWD.Net.Api.Divera.Exceptions;
using AMWD.Net.Api.Divera.Models;
using AMWD.Net.Api.Divera.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class CloseAlarmTests : TestBase
	{
		[TestMethod]
		public async Task ShouldMapFullRequest()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var request = new CloseAlarmRequest
			{
				Id = 123,
				IsClosed = true,
				Report = "That was nice",
				Timestamp = date
			};
			var api = GetApi(accessKey);

			// act
			bool response = await api.CloseAlarm(request);

			// assert
			Assert.IsTrue(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.CloseAlarm}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);
			var callback = HttpUtility.ParseQueryString(requestCallback.Content);

			Assert.AreEqual(request.Id, Convert<int?>(callback["Alarm[id]"]));
			Assert.AreEqual(request.IsClosed, Convert<bool?>(callback["Alarm[closed]"]));
			Assert.AreEqual(request.Report, callback["Alarm[report]"]);
			Assert.AreEqual(request.TimestampUnix, Convert<int?>(callback["Alarm[ts]"]));

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingRequest()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.CloseAlarm(null);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingId()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var request = new CloseAlarmRequest();

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.CloseAlarm(request);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnInvalidAccessKey()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.Forbidden;
			var request = new CloseAlarmRequest { Id = 123 };

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.CloseAlarm(request);
				Assert.Fail();
			}
			catch (AccessKeyException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;
			var request = new CloseAlarmRequest { Id = 123 };

			var api = GetApi(accessKey);

			// act
			string exceptionMessage = null;
			try
			{
				bool response = await api.CloseAlarm(request);
				Assert.Fail();
			}
			catch (DiveraException ex)
			{
				exceptionMessage = ex.Message;
			}

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.CloseAlarm}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);
			Assert.AreEqual($"HTTP error ({(int)httpResponseMessage.StatusCode}): ", exceptionMessage);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldReturnFalseOnDiveraError()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = false });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);
			var request = new CloseAlarmRequest { Id = 123 };

			var api = GetApi(accessKey);

			// act
			bool response = await api.CloseAlarm(request);

			// assert
			Assert.IsFalse(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.CloseAlarm}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public void ShouldConvertTimstampsCorrectly()
		{
			// arrange
			int unix = (int)date.Subtract(DateTime.UnixEpoch).TotalSeconds;
			var request = new CloseAlarmRequest();

			// act
			bool timestampHasValue = request.Timestamp.HasValue;
			bool unixHasValue = request.TimestampUnix.HasValue;

			request.TimestampUnix = unix;
			var getTimestamp = new DateTime(request.Timestamp.Value.Ticks);

			request.Timestamp = date.AddSeconds(1);
			int? getUnix = request.TimestampUnix;

			request.Timestamp = null;
			bool unixIsNull = !request.TimestampUnix.HasValue;

			// assert
			Assert.IsFalse(timestampHasValue);
			Assert.IsFalse(unixHasValue);

			Assert.AreEqual(date, getTimestamp);
			Assert.AreEqual(unix + 1, getUnix);

			Assert.IsTrue(unixIsNull);
		}
	}
}
