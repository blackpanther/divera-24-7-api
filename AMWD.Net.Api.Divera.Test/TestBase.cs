using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Unclassified.Util;

namespace AMWD.Net.Api.Divera.Test
{
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public abstract class TestBase
	{
		internal static readonly string jsonMimeType = "application/json";
		internal static readonly DateTime date = new(2022, 1, 9, 11, 22, 33, DateTimeKind.Utc);

		internal Mock<HttpMessageHandler> httpMessageHandlerMock;
		internal HttpClient httpClient;

		internal HttpRequestCallback requestCallback;
		internal HttpResponseMessage httpResponseMessage;

		[TestInitialize]
		public virtual void InitializeTest()
		{
			requestCallback = null;
			httpResponseMessage = new HttpResponseMessage
			{
				StatusCode = HttpStatusCode.OK
			};
		}

		[TestCleanup]
		public virtual void CleanupTest()
		{
			httpClient?.Dispose();
		}

		internal T Convert<T>(string str)
		{
			return (T)Convert(str, typeof(T));
		}

		internal object Convert(string str, Type baseType)
		{
			if (string.IsNullOrWhiteSpace(str))
				return default;

			var nullableType = Nullable.GetUnderlyingType(baseType);

			var type = nullableType == null ? baseType : nullableType;
			var converter = TypeDescriptor.GetConverter(type);

			if (converter == null)
				return default;

			if (type == typeof(bool))
			{
				object result;
				try
				{
					result = converter.ConvertFromInvariantString(str);
				}
				catch
				{
					result = DeepConvert.ChangeType(TypeDescriptor.GetConverter(typeof(int)).ConvertFromInvariantString(str), baseType);
				}

				return result;
			}

			if (baseType.IsAssignableTo(typeof(IList)))
			{
				var genericType = baseType.GenericTypeArguments.FirstOrDefault();
				if (genericType == null)
					return default;

				var list = (IList)Activator.CreateInstance(type);
				string[] split = str.Split(',');
				foreach (string s in split)
					list.Add(Convert(s, genericType));

				return list;
			}

			return converter.ConvertFromInvariantString(str);
		}

		internal DiveraApi GetApi(string accessKey)
		{
			httpMessageHandlerMock = new Mock<HttpMessageHandler>();
			httpMessageHandlerMock.Protected()
				.Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
				.Callback<HttpRequestMessage, CancellationToken>(async (req, _) =>
				{
					requestCallback = new HttpRequestCallback
					{
						Headers = req.Headers,
						Method = req.Method,
						Options = req.Options,
						RequestUri = req.RequestUri,
						Version = req.Version,
						VersionPolicy = req.VersionPolicy
					};

					if (req.Content != null)
						requestCallback.Content = await req.Content.ReadAsStringAsync();
				})
				.ReturnsAsync(httpResponseMessage);

			httpClient = new HttpClient(httpMessageHandlerMock.Object, disposeHandler: false);
			var api = new DiveraApi
			{
				AccessKey = accessKey
			};

			var field = api.GetType().GetField("httpClient", BindingFlags.NonPublic | BindingFlags.Instance);
			field.SetValue(api, httpClient);

			return api;
		}

		internal class HttpRequestCallback
		{
			public string Content { get; set; }

			public HttpRequestHeaders Headers { get; set; }

			public HttpMethod Method { get; set; }

			public HttpRequestOptions Options { get; set; }

			public Uri RequestUri { get; set; }

			public Version Version { get; set; }

			public HttpVersionPolicy VersionPolicy { get; set; }
		}
	}
}
