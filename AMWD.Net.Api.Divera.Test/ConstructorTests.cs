﻿using System;
using System.Threading.Tasks;
using AMWD.Net.Api.Divera.Exceptions;
using AMWD.Net.Api.Divera.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AMWD.Net.Api.Divera.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class ConstructorTests : TestBase
	{
		[TestMethod]
		public void ShouldCreateDefault()
		{
			// arrange

			// act
			using var api = new DiveraApi();

			// assert
			Assert.IsNotNull(api);
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnDisposed()
		{
			// arrange
			var request = new CloseAlarmRequest
			{
				Id = 123,
				Report = "Foo Bar"
			};
			var api = new DiveraApi();

			// act
			api.Dispose();

			// assert
			try
			{
				await api.CloseAlarm(request);
				Assert.Fail();
			}
			catch (ObjectDisposedException)
			{ /* expected behaviour */ }
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingAccessKey()
		{
			// arrange
			var request = new CloseAlarmRequest
			{
				Id = 123,
				Report = "Foo Bar"
			};
			using var api = new DiveraApi();

			// act + assert
			try
			{
				await api.CloseAlarm(request);
				Assert.Fail();
			}
			catch (AccessKeyException)
			{ /* expected behaviour */ }
		}
	}
}
