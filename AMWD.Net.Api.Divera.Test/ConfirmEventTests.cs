﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AMWD.Net.Api.Divera.Enums;
using AMWD.Net.Api.Divera.Exceptions;
using AMWD.Net.Api.Divera.Models;
using AMWD.Net.Api.Divera.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class ConfirmEventTests : TestBase
	{
		[TestMethod]
		public async Task ShouldMapFullRequest()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var request = new ConfirmEventRequest
			{
				EventId = 42,
				Type = EventConfirmType.NotSure,
				Message = "We'll see..."
			};
			var api = GetApi(accessKey);

			// act
			bool response = await api.ConfirmEvent(request);

			// assert
			Assert.IsTrue(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.ConfirmEvent}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);
			var callback = HttpUtility.ParseQueryString(requestCallback.Content);

			Assert.AreEqual(request.EventId, Convert<int?>(callback["Confirmation[event_id]"]));
			Assert.AreEqual((int)request.Type, Convert<int>(callback["Confirmation[type]"]));
			Assert.AreEqual(request.Message, callback["Confirmation[custom_answer]"]);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingRequest()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.ConfirmEvent(null);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingEventId()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var request = new ConfirmEventRequest
			{
				Type = EventConfirmType.Yes,
				Message = "Hell yeah!"
			};

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.ConfirmEvent(request);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingType()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var request = new ConfirmEventRequest
			{
				EventId = 4711,
				Message = "What?"
			};

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.ConfirmEvent(request);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnInvalidAccessKey()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.Forbidden;
			var request = new ConfirmEventRequest { EventId = 42, Type = EventConfirmType.NotSure };

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.ConfirmEvent(request);
				Assert.Fail();
			}
			catch (AccessKeyException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;
			var request = new ConfirmEventRequest { EventId = 42, Type = EventConfirmType.NotSure };

			var api = GetApi(accessKey);

			// act
			string exceptionMessage = null;
			try
			{
				bool response = await api.ConfirmEvent(request);
				Assert.Fail();
			}
			catch (DiveraException ex)
			{
				exceptionMessage = ex.Message;
			}

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.ConfirmEvent}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);
			Assert.AreEqual($"HTTP error ({(int)httpResponseMessage.StatusCode}): ", exceptionMessage);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldReturnFalseOnDiveraError()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = false });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);
			var request = new ConfirmEventRequest { EventId = 42, Type = EventConfirmType.NotSure };

			var api = GetApi(accessKey);

			// act
			bool response = await api.ConfirmEvent(request);

			// assert
			Assert.IsFalse(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.ConfirmEvent}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
