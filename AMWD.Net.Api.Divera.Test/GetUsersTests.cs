﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.Divera.Exceptions;
using AMWD.Net.Api.Divera.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class GetUsersTests : TestBase
	{
		[TestMethod]
		public async Task ShouldReturnFullResponse()
		{
			// arrange
			string accessKey = "abc";
			var list = new List<User>
			{
				new User
				{
					Id = 1,
					ClusterId = 10,
					ClusterRelationId = 11,
					Firstname = "1",
					Lastname = "11",
					StatusId = 20,
					ForeignId = "21"
				},
				new User
				{
					Id = 2,
					ClusterId = 10,
					ClusterRelationId = 12,
					Firstname = "2",
					Lastname = "22",
					StatusId = 20,
					ForeignId = "22"
				},
				new User
				{
					Id = 3,
					ClusterId = 10,
					ClusterRelationId = 13,
					Firstname = "3",
					Lastname = "33",
					StatusId = 20,
					ForeignId = "23"
				}
			};
			string responseJson = JsonConvert.SerializeObject(new Success<List<User>> { IsSuccess = true, Data = list });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var api = GetApi(accessKey);

			// act
			var response = await api.GetUsers();

			// assert
			Assert.IsNotNull(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.GetUsers}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Get, requestCallback.Method);

			Assert.IsNull(requestCallback.Content);

			for (int i = 0; i < list.Count; i++)
			{
				Assert.AreEqual(list[i].Id, response[i].Id);
				Assert.AreEqual(list[i].ClusterId, response[i].ClusterId);
				Assert.AreEqual(list[i].ClusterRelationId, response[i].ClusterRelationId);
				Assert.AreEqual(list[i].Firstname, response[i].Firstname);
				Assert.AreEqual(list[i].Lastname, response[i].Lastname);
				Assert.AreEqual(list[i].StatusId, response[i].StatusId);
				Assert.AreEqual(list[i].ForeignId, response[i].ForeignId);
			}

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnInvalidAccessKey()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.Forbidden;

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				var response = await api.GetUsers();
				Assert.Fail();
			}
			catch (AccessKeyException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;

			var api = GetApi(accessKey);

			// act
			string exceptionMessage = null;
			try
			{
				var response = await api.GetUsers();
				Assert.Fail();
			}
			catch (DiveraException ex)
			{
				exceptionMessage = ex.Message;
			}

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.GetUsers}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Get, requestCallback.Method);

			Assert.IsNull(requestCallback.Content);
			Assert.AreEqual($"HTTP error ({(int)httpResponseMessage.StatusCode}): ", exceptionMessage);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
