﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AMWD.Net.Api.Divera.Exceptions;
using AMWD.Net.Api.Divera.Models;
using AMWD.Net.Api.Divera.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class CreateAlarmTests : TestBase
	{
		[TestMethod]
		public async Task ShouldMapFullRequest()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var request = new CreateAlarmRequest
			{
				Number = "47123",
				Title = "Probealarm",
				HasPriority = true,
				Text = "Achtung! Dies ist ein Probealarm",
				Address = "Dorfstr. 15b, 89001 Musterstadt",
				Latitude = 49.12345f,
				Longitude = 11.12345f,
				SceneObject = "Haus",
				Caller = "Max Mustermann, +4917100001",
				Patient = "Maria Musterfrau",
				Remark = "Zugang nur über Dachluke",
				Units = "RTW, DLK",
				HasDestination = true,
				DestinationAddress = "Hospitalstraße 1, 89001 Musterhausen",
				DestinationLatitude = 48.12345f,
				DestinationLongitude = 10.12345f,
				AdditionalText1 = "Erster Zusatz",
				AdditionalText2 = "Zweiter Zusatz",
				AdditionalText3 = "Dritter Zusatz",
				Rics = new List<string> { "RTW", "DLK" },
				Vehicles = new List<string> { "RTW1", "DLK2" },
				Persons = new List<string> { "EFD" },
				DelaySeconds = 15,
				Coordinates = "gk",
				CoordinatesX = 12.3456f,
				CoordinatesY = 65.4321f,
				IsNews = false,
				Id = 123,
				ClusterIds = new List<int> { 1, 2 },
				GroupIds = new List<int> { 3 },
				UserClusterRelationIds = new List<int> { 4, 5 },
				VehicleIds = new List<int> { 6, 7 },
				AlarmcodeId = 8
			};
			var api = GetApi(accessKey);

			// act
			bool response = await api.CreateAlarm(request);

			// assert
			Assert.IsTrue(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.CreateAlarm}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);
			var callback = HttpUtility.ParseQueryString(requestCallback.Content);

			Assert.AreEqual(request.Number, callback["number"]);
			Assert.AreEqual(request.Title, callback["title"]);
			Assert.AreEqual(request.HasPriority, Convert<bool?>(callback["priority"]));
			Assert.AreEqual(request.Text, callback["text"]);
			Assert.AreEqual(request.Address, callback["address"]);
			Assert.AreEqual(request.Latitude, Convert<float?>(callback["lat"]));
			Assert.AreEqual(request.Longitude, Convert<float?>(callback["lng"]));
			Assert.AreEqual(request.SceneObject, callback["scene_object"]);
			Assert.AreEqual(request.Caller, callback["caller"]);
			Assert.AreEqual(request.Patient, callback["patient"]);
			Assert.AreEqual(request.Remark, callback["remark"]);
			Assert.AreEqual(request.Units, callback["units"]);
			Assert.AreEqual(request.HasDestination, Convert<bool?>(callback["destination"]));
			Assert.AreEqual(request.DestinationAddress, callback["destination_address"]);
			Assert.AreEqual(request.DestinationLatitude, Convert<float?>(callback["destination_lat"]));
			Assert.AreEqual(request.DestinationLongitude, Convert<float?>(callback["destination_lng"]));
			Assert.AreEqual(request.AdditionalText1, callback["additional_text_1"]);
			Assert.AreEqual(request.AdditionalText2, callback["additional_text_2"]);
			Assert.AreEqual(request.AdditionalText3, callback["additional_text_3"]);
			Assert.AreEqual(string.Join(",", request.Rics), callback["ric"]);
			Assert.AreEqual(string.Join(",", request.Vehicles), callback["vehicle"]);
			Assert.AreEqual(string.Join(",", request.Persons), callback["person"]);
			Assert.AreEqual(request.DelaySeconds, Convert<int?>(callback["delay"]));
			Assert.AreEqual(request.Coordinates, callback["coordinates"]);
			Assert.AreEqual(request.CoordinatesX, Convert<float?>(callback["coordinates-x"]));
			Assert.AreEqual(request.CoordinatesY, Convert<float?>(callback["coordinates-y"]));
			Assert.AreEqual(request.IsNews, Convert<bool?>(callback["news"]));
			Assert.AreEqual(request.Id, Convert<int?>(callback["id"]));
			CollectionAssert.AreEqual(request.ClusterIds.ToList(), Convert<List<int>>(callback["cluster_ids"]));
			CollectionAssert.AreEqual(request.GroupIds.ToList(), Convert<List<int>>(callback["group_ids"]));
			CollectionAssert.AreEqual(request.UserClusterRelationIds.ToList(), Convert<List<int>>(callback["user_cluster_relation_ids"]));
			CollectionAssert.AreEqual(request.VehicleIds.ToList(), Convert<List<int>>(callback["vehicle_ids"]));
			Assert.AreEqual(request.AlarmcodeId, Convert<int?>(callback["alarmcode_id"]));

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnMissingRequest()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = true });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.CreateAlarm(null);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(0), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnInvalidAccessKey()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.Forbidden;
			var request = new CreateAlarmRequest
			{
				Number = "47123",
				Title = "Probealarm",
				HasPriority = false
			};

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				bool response = await api.CreateAlarm(request);
				Assert.Fail();
			}
			catch (AccessKeyException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowErrorOnHttpError()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;
			var request = new CreateAlarmRequest
			{
				Number = "47123",
				Title = "Probealarm",
				HasDestination = false,
				Rics = null
			};

			var api = GetApi(accessKey);

			// act
			string exceptionMessage = null;
			try
			{
				bool response = await api.CreateAlarm(request);
				Assert.Fail();
			}
			catch (DiveraException ex)
			{
				exceptionMessage = ex.Message;
			}

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.CreateAlarm}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			Assert.IsNotNull(requestCallback.Content);
			Assert.AreEqual($"HTTP error ({(int)httpResponseMessage.StatusCode}): ", exceptionMessage);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldReturnFalseOnDiveraError()
		{
			// arrange
			string accessKey = "abc";
			string responseJson = JsonConvert.SerializeObject(new Success { IsSuccess = false });
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);
			var request = new CreateAlarmRequest
			{
				Number = "47123",
				Title = "Probealarm",
				HasDestination = true,
				DestinationAddress = "Hinterhau 1, 21345 Musterstadt"
			};

			var api = GetApi(accessKey);

			// act
			bool response = await api.CreateAlarm(request);

			// assert
			Assert.IsFalse(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.CreateAlarm}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Post, requestCallback.Method);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
