﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.Divera.Exceptions;
using AMWD.Net.Api.Divera.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;

namespace AMWD.Net.Api.Divera.Test
{
	[TestClass]
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class GetLatestAlarmTests : TestBase
	{
		[TestMethod]
		public async Task ShouldReturnFullResponse()
		{
			// arrange
			string accessKey = "abc";
			var alarmResponse = new AlarmResult
			{
				Title = "Probealarm",
				HasPriority = true,
				Text = "Achtung! Dies ist ein Probealarm",
				Address = "Dorfstr. 15b, 89001 Musterstadt",
				Latitude = 49.12345f,
				Longitude = 11.12345f,
				SceneObject = "Haus",
				Caller = "Max Mustermann, +4917100001",
				Patient = "Maria Musterfrau",
				Remark = "Zugang nur über Dachluke",
				Units = "RTW, DLK",
				HasDestination = true,
				DestinationAddress = "Hospitalstraße 1, 89001 Musterhausen",
				DestinationLatitude = 48.12345f,
				DestinationLongitude = 10.12345f,
				AdditionalText1 = "Erster Zusatz",
				AdditionalText2 = "Zweiter Zusatz",
				AdditionalText3 = "Dritter Zusatz",
				Id = 123,
				ClusterIds = new List<int> { 21, 42 },
				GroupIds = new List<int> { 13 },
				UserClusterRelationIds = new List<int> { 2113, 4213 },
				VehicleIds = new List<int> { 7, 9 },
				AlarmcodeId = 2,
				AuthorId = 815,
				CloseTimestampUnix = 123456,
				CreateTimestampUnix = 234567,
				Date = date,
				DoSendCall = true,
				DoSendMail = false,
				DoSendPager = true,
				DoSendPush = false,
				DoSendSms = true,
				ForeignId = "fk-123",
				HasNotificationFilterAccess = false,
				HasNotificationFilterStatus = true,
				HasNotificationFilterVehicle = false,
				IsAnswerable = true,
				IsClosed = false,
				IsDeleted = true,
				IsDraft = false,
				IsEditable = true,
				IsNew = false,
#pragma warning disable CS0618
				IsNotificationFilterStatusAccess = true,
#pragma warning restore CS0618
				IsPrivateMode = false,
				IsSelfAddressed = true,
				NotificationFilterStatusIds = new List<int> { 1, 2, 3 },
				NotificationType = Enums.DiveraNotificationType.SelectedUsers,
				PublishTimestampUnix = 345678,
				ReadCount = 5,
				RecipientsCount = 10,
				Report = "Hilfe, die Welt geht unter",
				ResponseTimeMinutes = 12,
				ResponseTimestampUnix = 456789,
				SelfNote = "Hahaha",
				SelfStatusId = 6,
				UpdateTimestampUnix = 567890,
				UserIdsAddressed = new List<int> { 2, 3, 4 },
				UserIdsAnswered = new List<int> { 2, 4 }
			};
			string responseJson = JsonConvert.SerializeObject(alarmResponse);
			httpResponseMessage.Content = new StringContent(responseJson, Encoding.UTF8, jsonMimeType);

			var api = GetApi(accessKey);

			// act
			var response = await api.GetLatestAlarm();

			// assert
			Assert.IsNotNull(response);
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.GetLatestAlarm}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Get, requestCallback.Method);

			Assert.IsNull(requestCallback.Content);

			Assert.AreEqual(alarmResponse.AdditionalText1, response.AdditionalText1);
			Assert.AreEqual(alarmResponse.AdditionalText2, response.AdditionalText2);
			Assert.AreEqual(alarmResponse.AdditionalText3, response.AdditionalText3);
			Assert.AreEqual(alarmResponse.Address, response.Address);
			Assert.AreEqual(alarmResponse.AlarmcodeId, response.AlarmcodeId);
			Assert.AreEqual(alarmResponse.AuthorId, response.AuthorId);
			Assert.AreEqual(alarmResponse.Caller, response.Caller);
			Assert.AreEqual(alarmResponse.CloseTimestampUnix, response.CloseTimestampUnix);
			CollectionAssert.AreEqual(alarmResponse.ClusterIds, response.ClusterIds);
			Assert.AreEqual(alarmResponse.CreateTimestampUnix, response.CreateTimestampUnix);
			Assert.AreEqual(alarmResponse.DateUnix, response.DateUnix);
			Assert.AreEqual(alarmResponse.DestinationAddress, response.DestinationAddress);
			Assert.AreEqual(alarmResponse.DestinationLatitude, response.DestinationLatitude);
			Assert.AreEqual(alarmResponse.DestinationLongitude, response.DestinationLongitude);
			Assert.AreEqual(alarmResponse.DoSendCall, response.DoSendCall);
			Assert.AreEqual(alarmResponse.DoSendMail, response.DoSendMail);
			Assert.AreEqual(alarmResponse.DoSendPager, response.DoSendPager);
			Assert.AreEqual(alarmResponse.DoSendPush, response.DoSendPush);
			Assert.AreEqual(alarmResponse.DoSendSms, response.DoSendSms);
			Assert.AreEqual(alarmResponse.ForeignId, response.ForeignId);
			CollectionAssert.AreEqual(alarmResponse.GroupIds, response.GroupIds);
			Assert.AreEqual(alarmResponse.HasDestination, response.HasDestination);
			Assert.AreEqual(alarmResponse.HasNotificationFilterAccess, response.HasNotificationFilterAccess);
			Assert.AreEqual(alarmResponse.HasNotificationFilterStatus, response.HasNotificationFilterStatus);
			Assert.AreEqual(alarmResponse.HasNotificationFilterVehicle, response.HasNotificationFilterVehicle);
			Assert.AreEqual(alarmResponse.HasPriority, response.HasPriority);
			Assert.AreEqual(alarmResponse.Id, response.Id);
			Assert.AreEqual(alarmResponse.IsAnswerable, response.IsAnswerable);
			Assert.AreEqual(alarmResponse.IsClosed, response.IsClosed);
			Assert.AreEqual(alarmResponse.IsDeleted, response.IsDeleted);
			Assert.AreEqual(alarmResponse.IsDraft, response.IsDraft);
			Assert.AreEqual(alarmResponse.IsEditable, response.IsEditable);
			Assert.AreEqual(alarmResponse.IsNew, response.IsNew);
#pragma warning disable CS0618
			Assert.AreEqual(alarmResponse.IsNotificationFilterStatusAccess, response.IsNotificationFilterStatusAccess);
#pragma warning restore CS0618
			Assert.AreEqual(alarmResponse.IsPrivateMode, response.IsPrivateMode);
			Assert.AreEqual(alarmResponse.IsSelfAddressed, response.IsSelfAddressed);
			Assert.AreEqual(alarmResponse.Latitude, response.Latitude);
			Assert.AreEqual(alarmResponse.Longitude, response.Longitude);
			CollectionAssert.AreEqual(alarmResponse.NotificationFilterStatusIds, response.NotificationFilterStatusIds);
			Assert.AreEqual(alarmResponse.NotificationType, response.NotificationType);
			Assert.AreEqual(alarmResponse.Patient, response.Patient);
			Assert.AreEqual(alarmResponse.PublishTimestampUnix, response.PublishTimestampUnix);
			Assert.AreEqual(alarmResponse.ReadCount, response.ReadCount);
			Assert.AreEqual(alarmResponse.RecipientsCount, response.RecipientsCount);
			Assert.AreEqual(alarmResponse.Remark, response.Remark);
			Assert.AreEqual(alarmResponse.Report, response.Report);
			Assert.AreEqual(alarmResponse.ResponseTimeMinutes, response.ResponseTimeMinutes);
			Assert.AreEqual(alarmResponse.ResponseTimestampUnix, response.ResponseTimestampUnix);
			Assert.AreEqual(alarmResponse.SceneObject, response.SceneObject);
			Assert.AreEqual(alarmResponse.SelfNote, response.SelfNote);
			Assert.AreEqual(alarmResponse.SelfStatusId, response.SelfStatusId);
			Assert.AreEqual(alarmResponse.Text, response.Text);
			Assert.AreEqual(alarmResponse.Title, response.Title);
			Assert.AreEqual(alarmResponse.Units, response.Units);
			Assert.AreEqual(alarmResponse.UpdateTimestampUnix, response.UpdateTimestampUnix);
			CollectionAssert.AreEqual(alarmResponse.UserClusterRelationIds, response.UserClusterRelationIds);
			CollectionAssert.AreEqual(alarmResponse.UserIdsAddressed, response.UserIdsAddressed);
			CollectionAssert.AreEqual(alarmResponse.UserIdsAnswered, response.UserIdsAnswered);
			CollectionAssert.AreEqual(alarmResponse.VehicleIds, response.VehicleIds);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnInvalidAccessKey()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.Forbidden;

			var api = GetApi(accessKey);

			// act + assert
			try
			{
				var response = await api.GetLatestAlarm();
				Assert.Fail();
			}
			catch (AccessKeyException)
			{ /* expected behaviour */ }

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}

		[TestMethod]
		public async Task ShouldThrowExceptionOnHttpError()
		{
			// arrange
			string accessKey = "abc";
			httpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;

			var api = GetApi(accessKey);

			// act
			string exceptionMessage = null;
			try
			{
				var response = await api.GetLatestAlarm();
				Assert.Fail();
			}
			catch (DiveraException ex)
			{
				exceptionMessage = ex.Message;
			}

			// assert
			Assert.IsNotNull(requestCallback);
			Assert.AreEqual($"{DiveraApiUrls.GetLatestAlarm}?accesskey={accessKey}", requestCallback.RequestUri.ToString());
			Assert.AreEqual(HttpMethod.Get, requestCallback.Method);

			Assert.IsNull(requestCallback.Content);
			Assert.AreEqual($"HTTP error ({(int)httpResponseMessage.StatusCode}): ", exceptionMessage);

			httpMessageHandlerMock.Protected().Verify("SendAsync", Times.Exactly(1), ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
		}
	}
}
